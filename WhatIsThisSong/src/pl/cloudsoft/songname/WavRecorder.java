package pl.cloudsoft.songname;

import static android.media.AudioFormat.CHANNEL_IN_MONO;
import static android.media.AudioFormat.ENCODING_PCM_16BIT;
import static android.media.AudioRecord.STATE_INITIALIZED;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.FrameRecorder;
import org.bytedeco.javacv.FrameRecorder.Exception;

import android.media.AudioRecord;
import android.util.Log;

public class WavRecorder {

    private static final String TAG = WavRecorder.class.getSimpleName();

    private static final int N_CHANNELS = 1;
    private static final int CHANNEL_CONFIG = CHANNEL_IN_MONO;
    private static final int SAMPLES_FORMAT = ENCODING_PCM_16BIT;
    private static final String AUDIO_FORMAT = "wav";

    private int bufferSize;
    private int inputSampleRate;

    private AudioRecord audioRecord;
    private FrameRecorder frameRecorder;

    private Thread thread;
    private volatile boolean stop;

    public WavRecorder(int audioSource, int inputSampleRate, int outputSampleRate, String filename) {
        bufferSize = AudioRecord.getMinBufferSize(inputSampleRate, CHANNEL_CONFIG, SAMPLES_FORMAT);
        this.inputSampleRate = inputSampleRate;

        audioRecord = new AudioRecord(audioSource, inputSampleRate, CHANNEL_CONFIG, SAMPLES_FORMAT, bufferSize);
        if (audioRecord.getState() != STATE_INITIALIZED) {
            throw new IllegalArgumentException("could not initialize AudioRecord");
        }

        frameRecorder = new FFmpegFrameRecorder(filename, N_CHANNELS);
        frameRecorder.setSampleRate(outputSampleRate);
        frameRecorder.setFormat(AUDIO_FORMAT);
    }

    public static WavRecorder recorder(int audioSource, int outputSampleRate, String filename, int... inputSampleRates) {
        for (int i = 0; i < inputSampleRates.length; ++i) {
            int inputSampleRate = inputSampleRates[i];
            try {
                return new WavRecorder(audioSource, inputSampleRate, outputSampleRate, filename);
            } catch (IllegalArgumentException e) {
                Log.i(TAG, "Could not initialize recorder for input sample rate: " + inputSampleRate);
            }
        }
        throw new IllegalArgumentException("could not initialize AudioRecord");
    }

    public void start() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    frameRecorder.start();
                    audioRecord.startRecording();

                    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize);
                    ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
                    while (!stop) {
                        int nRead = audioRecord.read(byteBuffer, bufferSize);
                        shortBuffer.position(0).limit(nRead / 2);
                        frameRecorder.record(inputSampleRate, N_CHANNELS, shortBuffer);
                    }

                    audioRecord.stop();
                    audioRecord.release();
                    frameRecorder.stop();
                } catch (Exception e) {
                    Log.e(TAG, "could not record audio", e);
                }
            }
        });
        thread.start();
    }

    public void stop() throws InterruptedException {
        stop = true;
        thread.join();
    }
}
