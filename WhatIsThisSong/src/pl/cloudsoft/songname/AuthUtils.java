package pl.cloudsoft.songname;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;

public final class AuthUtils {

    private static final String SCOPE = "audience:server:client_id:677122513805.apps.googleusercontent.com";

    public static String[] getAccountNames(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        String[] names = new String[accounts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = accounts[i].name;
        }
        return names;
    }

    public static String getDefaultAccountName(Context context) {
        String[] accountNames = getAccountNames(context);
        return accountNames.length > 0 ? accountNames[0] : null;
    }

    public static String getToken(Context context) throws GoogleAuthException, IOException {
        String accountName = getDefaultAccountName(context);
        if (accountName == null) {
            throw new GoogleAuthException("No user accounts");
        }
        return GoogleAuthUtil.getTokenWithNotification(context, accountName, SCOPE, null);
    }

    private AuthUtils() {
    }
}
