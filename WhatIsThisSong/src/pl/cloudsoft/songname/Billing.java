package pl.cloudsoft.songname;

import pl.cloudsoft.songname.billing.IabHelper;
import pl.cloudsoft.songname.billing.IabResult;
import pl.cloudsoft.songname.billing.Inventory;
import pl.cloudsoft.songname.billing.Purchase;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class Billing {

    private static final String TAG = Billing.class.getSimpleName();

    private static final String BILLING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCg"
            + "KCAQEAyBqVdPT1ZiD1YzlQfILOnuCQs5wkJs6Yo/C7qglN+jMNes9dfh3aXb2HgxbMx0Ze186fQg"
            + "dmesIqCdHUQfNJbikS4pauBPRjfkJcapAZQdq3O3zsJGBc7RKrAms7t64Apmq13xd60hMZAoaba4"
            + "XaZrMEXBGSo0jDUZYoaVrr6fkKJ1pOJHV303hN5+IpN0kgZGPX7mBrd68rEdxaot7J7DkvXBeVoO"
            + "J2t6T2xILPCPgTOU7q9tUaYX6P1F6ZfIA1+5u4yXZr/YuALpNwk3xImLMR9gms/f45L1XFQsEBI2"
            + "iPoN18LEO7r+mNUFEsep/1FGSUZwLcWlBsIm6rO80wqwIDAQAB";

    private static final String SKU_PREMIUM = "android.test.purchased";

    private Activity activity;
    private IabHelper billingHelper;
    private boolean hasPremium;

    public Billing(Activity activity) {
        this.activity = activity;
        this.billingHelper = new IabHelper(activity, BILLING_KEY);
    }

    public void initialize() {
        billingHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (result.isFailure()) {
                    Log.i(TAG, "Problem setting up In-app Billing: " + result);
                    return;
                }
                queryPremium();
            }
        });
    }

    public boolean hasPremium() {
        return hasPremium;
    }

    public void purchasePremium() {
        IabHelper.OnIabPurchaseFinishedListener purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                boolean alreadyOwned = result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED;
                if (result.isFailure() && !alreadyOwned) {
                    Log.i(TAG, "Error purchasing: " + result);
                    return;
                }
                hasPremium = alreadyOwned || purchase.getSku().equals(SKU_PREMIUM);
            }
        };
        billingHelper.launchPurchaseFlow(activity, SKU_PREMIUM, 10001, purchaseFinishedListener,
                AuthUtils.getDefaultAccountName(activity));
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return billingHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public void dispose() {
        billingHelper.dispose();
    }

    private void queryPremium() {
        billingHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                if (result.isFailure()) {
                    Log.i(TAG, "Could not obtain inventory: " + result);
                    return;
                }
                hasPremium = inventory.hasPurchase(SKU_PREMIUM);
            }
        });
    }
}
