package pl.cloudsoft.songname;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

public final class SongAppLauncher {

    private static final String YOUTUBE_PACKAGE_NAME = "com.google.android.youtube";
    private static final String PACKAGE_NAME_KEY = "packageName";
    private static final String NAME_KEY = "name";

    private Activity activity;
    private Billing billing;

    public SongAppLauncher(Activity activity, Billing billing) {
        this.activity = activity;
        this.billing = billing;
    }

    public void openSongInApp(final String songURL) {
        // try to open in saved app first
        if (openSongInStoredApp(songURL)) {
            return;
        }
        // list all apps capable of opening song url
        final List<ResolveInfo> songApps = retrieveSongApps(songURL);
        if (songApps.isEmpty()) {
            return;
        }
        final int youtubeAppPos = Math.max(findYoutubeApp(songApps), 0);
        // get apps icons and labels
        List<CharSequence> labels = new ArrayList<CharSequence>(songApps.size());
        List<Drawable> icons = new ArrayList<Drawable>(songApps.size());
        for (ResolveInfo songApp : songApps) {
            labels.add(songApp.loadLabel(activity.getPackageManager()));
            icons.add(songApp.loadIcon(activity.getPackageManager()));
        }
        // build and show app selection dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getResources().getString(R.string.open_with));
        builder.setSingleChoiceItems(new ArrayAdapterWithIcon(activity, labels, icons), youtubeAppPos, null);
        builder.setPositiveButton(activity.getResources().getString(R.string.open_once), null);
        builder.setNegativeButton(activity.getResources().getString(R.string.open_always), null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        justOnceButtonClick(alertDialog, songURL, songApps, youtubeAppPos);
                    }
                });
                button = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alwaysButtonClick(alertDialog, songURL, songApps, youtubeAppPos);
                    }
                });
            }
        });
        alertDialog.show();
    }

    public void removeStoredSongApp() {
        SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.remove(PACKAGE_NAME_KEY);
        editor.remove(NAME_KEY);
        editor.commit();
    }

    private void justOnceButtonClick(AlertDialog alertDialog, String songURL, List<ResolveInfo> songApps,
            int youtubeAppPos) {
        // get selected song app index
        int selectedSongAppPos = alertDialog.getListView().getCheckedItemPosition();
        if (selectedSongAppPos != youtubeAppPos && !billing.hasPremium()) {
            billing.purchasePremium();
            return;
        }
        // open song in selected app
        openSongInSelectedApp(songApps.get(selectedSongAppPos), songURL);
        alertDialog.dismiss();
    }

    private void alwaysButtonClick(AlertDialog alertDialog, String songURL, List<ResolveInfo> songApps,
            int youtubeAppPos) {
        // get selected song app index
        int selectedAppPos = alertDialog.getListView().getCheckedItemPosition();
        if (selectedAppPos != youtubeAppPos && !billing.hasPremium()) {
            billing.purchasePremium();
            return;
        }
        // open song in selected app and store selection
        openSongInSelectedApp(songApps.get(selectedAppPos), songURL);
        storeSelectedSongApp(songApps.get(selectedAppPos));
        alertDialog.dismiss();
    }

    private void storeSelectedSongApp(ResolveInfo songApp) {
        ActivityInfo activityInfo = songApp.activityInfo;
        SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(PACKAGE_NAME_KEY, activityInfo.applicationInfo.packageName);
        editor.putString(NAME_KEY, activityInfo.name);
        editor.commit();
    }

    private boolean openSongInStoredApp(String songURL) {
        try {
            // check if there is a stored app
            SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
            if (!preferences.contains(PACKAGE_NAME_KEY) || !preferences.contains(NAME_KEY)) {
                return false;
            }
            // open song url in a stored app
            String packageName = preferences.getString(PACKAGE_NAME_KEY, null);
            String name = preferences.getString(NAME_KEY, null);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(songURL));
            intent.setClassName(packageName, name);
            activity.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException ex) {
            return false;
        }
    }

    private void openSongInSelectedApp(ResolveInfo songApp, String songURL) {
        // open song url in a selected app
        ActivityInfo activityInfo = songApp.activityInfo;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(songURL));
        intent.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
        activity.startActivity(intent);
    }

    private int findYoutubeApp(List<ResolveInfo> apps) {
        for (int i = 0; i < apps.size(); ++i) {
            if (apps.get(i).activityInfo.applicationInfo.packageName.equals(YOUTUBE_PACKAGE_NAME)) {
                return i;
            }
        }
        return -1;
    }

    private List<ResolveInfo> retrieveSongApps(String songURL) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(songURL));
        return activity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    }
}
