package pl.cloudsoft.songname;

import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterWithIcon extends ArrayAdapter<CharSequence> {

    private List<Drawable> images;

    public ArrayAdapterWithIcon(Context context, List<CharSequence> items, List<Drawable> images) {
        super(context, android.R.layout.select_dialog_singlechoice, items);
        this.images = images;
    }

    public ArrayAdapterWithIcon(Context context, CharSequence[] items, Drawable[] images) {
        super(context, android.R.layout.select_dialog_singlechoice, items);
        this.images = Arrays.asList(images);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setCompoundDrawablesWithIntrinsicBounds(images.get(position), null, null, null);
        textView.setCompoundDrawablePadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12,
                getContext().getResources().getDisplayMetrics()));
        return view;
    }
}