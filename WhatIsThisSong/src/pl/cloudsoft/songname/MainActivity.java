package pl.cloudsoft.songname;

import static android.media.MediaRecorder.AudioSource.MIC;
import static com.youtube2mp3.wang.LandmarkConsts.SAMPLE_RATE;
import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.entity.ContentType.DEFAULT_BINARY;

import java.io.IOException;
import java.util.Collection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.youtube2mp3.wang.LandmarksCalculator.Landmark;
import com.youtube2mp3.wang.LandmarksCalculatorFactory;
import com.youtube2mp3.wang.LandmarksUtils;
import com.youtube2mp3.wang.WangLandmarksCalculatorFactory;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int[] MIC_SAMPLE_RATES = new int[] { 11025, 44100, 22050, 16000 };
    private static final long MIN_RECORDING_TIME = 5 * 1000L;
    private static final long MAX_RECORDING_TIME = 15 * 1000L;

    private static final String QUERY_URL = "https://jakapiosenka.pl/api/query_landmarks/";
    // private static final String QUERY_URL =
    // "http://192.168.1.5:8000/api/query_landmarks/";
    private static final int CONNECTION_TIMEOUT = 10 * 1000;

    private ToggleButton recordButton;
    private TextView status;
    private TextView retry;
    private TextView result;
    private Animation blinkAnimation;

    private WavRecorder recorder;
    private String recordingFileName;
    private long recordingStart;
    private Handler handler;
    private LandmarksCalculatorFactory calculatorFactory;

    private HttpClient client;
    private String songURL;

    private Billing billing;
    private SongAppLauncher songAppLauncher;

    static {
        Loader.load(opencv_core.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recordButton = (ToggleButton) findViewById(R.id.recButton);
        status = (TextView) findViewById(R.id.status);
        retry = (TextView) findViewById(R.id.retry);
        result = (TextView) findViewById(R.id.result);
        blinkAnimation = AnimationUtils.loadAnimation(this, R.anim.blink);
        recordingFileName = getCacheDir().getAbsolutePath() + "/rec.wav";
        handler = new Handler();
        calculatorFactory = new WangLandmarksCalculatorFactory();
        client = UntrustedSSLSocketFactory.getUntrustedHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), CONNECTION_TIMEOUT);
        billing = new Billing(this);
        billing.initialize();
        songAppLauncher = new SongAppLauncher(this, billing);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        billing.dispose();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.remove_stored_song_app:
            songAppLauncher.removeStoredSongApp();
            break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // pass on the activity result to the billing helper for handling
        if (!billing.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onRecClicked(View view) {
        if (!recordButton.isChecked()) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    public void onRetryClick(View v) {
        recognizeRequest();
    }

    public void onLinkClick(View v) {
        // open app selection context menu
        if (songURL != null) {
            songAppLauncher.openSongInApp(songURL);
        }
    }

    private void startRecording() {
        try {
            recorder = WavRecorder.recorder(MIC, SAMPLE_RATE, recordingFileName, MIC_SAMPLE_RATES);
            recorder.start();

            startRecTimer();
            setRecognizedSong("", null);
            setRecordButtonChecked(true);
            setDimEnabled(false);
        } catch (Exception exception) {
            Log.e(TAG, "could not start recording", exception);
            setErrorStatus(getString(R.string.error_could_not_record));
        }
    }

    private void stopRecording() {
        // stop recording
        handler.removeCallbacks(recTimer);
        try {
            recorder.stop();
        } catch (Exception exception) {
            Log.i(TAG, "stop failed", exception);
        } finally {
            recorder = null;
        }
        // recognize song if recording reached minimal duration
        if (getRecDuration() > MIN_RECORDING_TIME) {
            recognizeRequest();
        } else {
            setStatus(getString(R.string.start_recording));
            setRecordButtonChecked(false);
            setDimEnabled(true);
        }
    }

    private String formatRecognizedSong(JSONArray songNameParts) throws JSONException {
        String songName;
        if (songNameParts.length() == 3) {
            // song name\n artist\n album
            songName = songNameParts.getString(2) + "\n" + songNameParts.getString(0) + "\n"
                    + songNameParts.getString(1);
        } else {
            // each part in newline
            StringBuilder songNameBuilder = new StringBuilder();
            for (int i = 0; i < songNameParts.length(); ++i) {
                String songNamePart = songNameParts.getString(i);
                songNameBuilder.append(i > 0 ? "\n" + songNamePart : songNamePart);
            }
            songName = songNameBuilder.toString();
        }
        return songName;
    }

    private Collection<Landmark> computeLandmarks() throws Exception {
        long startTime = System.currentTimeMillis();
        Collection<Landmark> landmarks = LandmarksUtils.landmarksForFile(recordingFileName, calculatorFactory);
        long endTime = System.currentTimeMillis();
        Log.i(TAG, "Landmarks computation time: " + (endTime - startTime));
        return landmarks;
    }

    private int makeQuery(String token, byte[] landmarksBytes) throws Exception {
        // make recognize query
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        Log.i(TAG, "landmarks size: " + landmarksBytes.length);
        builder.addBinaryBody("landmarks", landmarksBytes, DEFAULT_BINARY, "landmarks");
        builder.addTextBody("token", token);
        HttpPost post = new HttpPost(QUERY_URL);
        post.setEntity(builder.build());
        HttpResponse response = client.execute(post);
        HttpEntity entity = response.getEntity();
        String responseString = entity != null ? EntityUtils.toString(response.getEntity()) : null;
        // check response status code
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != SC_OK) {
            return statusCode;
        }
        // parse response JSON
        JSONObject result = new JSONObject(responseString);
        // check response status
        if (!result.getString("status").equals("ok")) {
            throw new IOException("invalid response status");
        }
        JSONArray songs = result.getJSONArray("results");
        if (songs.length() >= 1) {
            JSONObject song = (JSONObject) songs.get(0);
            JSONArray songNameParts = song.getJSONArray("name");
            String songName = formatRecognizedSong(songNameParts);
            String songURL = song.getString("url");
            setStatus(getString(R.string.found_match));
            setRecognizedSong(songName, songURL);
        } else {
            // no songs recognized
            setStatus(getString(R.string.not_found_match));
        }
        return SC_OK;
    }

    private void recognizeRequest() {
        setStatus(getString(R.string.recognizing));
        setRecordButtonChecked(true);
        setRecordButtonEnabled(false);
        setRecordButtonAnimation(true);
        setDimEnabled(false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] landmarksBytes = LandmarksUtils.serializeLandmarks(computeLandmarks());
                    // generate token
                    String token = AuthUtils.getToken(MainActivity.this);
                    int statusCode = makeQuery(token, landmarksBytes);
                    if (statusCode != SC_OK) {
                        // try with new token
                        if (statusCode == SC_FORBIDDEN) {
                            Log.i(TAG, "generating new token");
                            GoogleAuthUtil.invalidateToken(MainActivity.this, token);
                            token = AuthUtils.getToken(MainActivity.this);
                            statusCode = makeQuery(token, landmarksBytes);
                        }
                        // verify final result
                        if (statusCode != SC_OK) {
                            throw new IOException("invalid response status code: " + statusCode);
                        }
                    }
                } catch (Exception exception) {
                    Log.e(TAG, "an exception occured", exception);
                    setErrorStatus(getString(R.string.error_connection_failed));
                }

                setRecordButtonChecked(false);
                setRecordButtonAnimation(false);
                setRecordButtonEnabled(true);
                setDimEnabled(true);
            }
        }).start();
    }

    private long getRecDuration() {
        long currTime = System.currentTimeMillis();
        return currTime - recordingStart;
    }

    private Runnable recTimer = new Runnable() {
        @Override
        public void run() {
            // update status label text
            long recDuration = getRecDuration();
            setStatus(getString(R.string.recording) + (recDuration / 1000) + "s");
            if (recDuration > MAX_RECORDING_TIME) {
                stopRecording();
            } else {
                handler.postDelayed(this, 100);
            }
        }
    };

    private void startRecTimer() {
        recordingStart = System.currentTimeMillis();
        recTimer.run();
    }

    private void setRecordButtonChecked(final boolean checked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordButton.setChecked(checked);
            }
        });
    }

    private void setRecordButtonEnabled(final boolean enabled) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordButton.setEnabled(enabled);
            }
        });
    }

    private void setRecordButtonAnimation(final boolean enabled) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordButton.setAnimation(enabled ? blinkAnimation : null);
            }
        });
    }

    private void setRecognizedSong(final String songName, final String songURL) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MainActivity.this.songURL = songURL;
                result.setText(songName);
            }
        });
    }

    private void setStatus(final String statusMsg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setTextColor(getResources().getColor(R.color.black));
                status.setText(statusMsg);
                retry.setVisibility(View.GONE);
            }
        });
    }

    private void setErrorStatus(final String statusMsg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setTextColor(getResources().getColor(R.color.red));
                status.setText(statusMsg);
                if (statusMsg.equals(getString(R.string.error_connection_failed))) {
                    retry.setVisibility(View.VISIBLE);
                } else {
                    retry.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setDimEnabled(final boolean enabled) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!enabled) {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                } else {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }
        });
    }

    public static class ProgrammaticToggleButton extends ToggleButton {
        public ProgrammaticToggleButton(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public void toggle() {
        }
    }
}
