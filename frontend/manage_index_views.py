# coding=utf-8
'''
Created on 17 sty 2014

@author: sopel39
'''
from datetime import datetime, timedelta
import json
import logging

from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import render
from tzlocal import get_localzone

from database.recordingsindex import RecordingsIndex, Recording
from database.youtube import add_youtube_fingerprint
from settings.paths import TMP_BACKEND
from utils.solr import datetime_to_solr, solr_escape
from utils.string import str_to_tuple
from utils.temporarydir import TemporaryDirectory
from utils.youtube import get_youtube_title, extract_youtube_id
from wang import tasks as fingerprint


logger = logging.getLogger(__name__)

@admin.site.admin_view
def manage_index(request, template_name):
    return render(request, template_name)

@admin.site.admin_view
def get_recordings(request):
    params = request.GET
    start = int(params['iDisplayStart'])
    rows = int(params['iDisplayLength'])
    # extract sorting column
    sort = RecordingsIndex.RECORDING_ID_COL + ' asc'
    if 'iSortCol_0' in params:
        sort_column = int(params['iSortCol_0'])
        if sort_column == 0:
            sort = RecordingsIndex.RECORDING_NAME_SORT_COL
        elif sort_column == 1:
            sort = RecordingsIndex.RECORDING_TYPE_COL
        elif sort_column == 2:
            sort = RecordingsIndex.RECORDING_CREATEDATE_COL
        elif sort_column == 3:
            sort = RecordingsIndex.MATCHES_COUNT_COL
        else:
            raise Exception("invalid iSortCol_0")
        sort += ' ' + params['sSortDir_0']
    # name search field
    query = []
    if 'sSearch_0' in params and params['sSearch_0']:
        terms = ['+' + solr_escape(term) for term in params['sSearch_0'].split()]
        query.append(RecordingsIndex.RECORDING_NAME_COL + ':(' + (' '.join(terms)) + ')')
    if 'sSearch_1' in params and params['sSearch_1']:
        query.append(RecordingsIndex.RECORDING_TYPE_COL + ':' + params['sSearch_1'])
    if 'sSearch_2' in params and params['sSearch_2']:
        search_date = get_localzone().localize(datetime.strptime(params['sSearch_2'], '%d/%m/%Y'))
        query.append(RecordingsIndex.RECORDING_CREATEDATE_COL + ':[' + datetime_to_solr(search_date)
                     + " TO " + datetime_to_solr(search_date + timedelta(days=1)) + "}")
    if 'sSearch_3' in params and params['sSearch_3']:
        query.append(RecordingsIndex.MATCHES_COUNT_COL + ':' + params['sSearch_3'])
    query = ' AND '.join(query)
    # build dataTables response
    response = {'sEcho': int(params['sEcho'])}
    with RecordingsIndex() as index:
        data = []
        try:
            results = index.get_recordings(start=start, rows=rows, and_query=query, sort=sort)
            recordings = results[RecordingsIndex.RESULTS]
            response['iTotalRecords'] = index.get_total_recordings()
            response['iTotalDisplayRecords'] = results[RecordingsIndex.NUM_FOUND]
            for recording in recordings:
                data.append({
                    'DT_RowId': recording[RecordingsIndex.ID],
                    '0': str_to_tuple(recording[RecordingsIndex.NAME]),
                    '1': recording[RecordingsIndex.TYPE],
                    '2': recording[RecordingsIndex.CREATEDATE].strftime('%d/%m/%y'),
                    '3': len(recording[RecordingsIndex.MATCHES])
                })
        except:
            pass
        response['aaData'] = data
    return HttpResponse(json.dumps(response))

@admin.site.admin_view
def get_matches(request, recording_id):
    # build dataTables response
    response = {}
    with RecordingsIndex() as index:
        data = []
        try:
            recording = index.get_recording(recording_id)
            if recording:
                for match_id, match in recording[RecordingsIndex.MATCHES].items():
                    data.append({
                        'DT_RowId': match_id,
                        '0': match[RecordingsIndex.NAME],
                        '1': match[RecordingsIndex.TYPE],
                        '2': match[RecordingsIndex.URL],
                        '3': match[RecordingsIndex.CREATEDATE].strftime('%d/%m/%y'),
                        '4': match[RecordingsIndex.IGNORE]
                    })
        except:
            pass
        response['aaData'] = data
    return HttpResponse(json.dumps(response))

@admin.site.admin_view
def add_youtube_recording(request):
    try:
        youtube_url = request.GET['youtube_url']
        youtube_id = extract_youtube_id(youtube_url)
        youtube_title = get_youtube_title(youtube_id)
        recording_id = 'youtube_' + youtube_id
        recording_name = request.GET['recording_name']
        if not recording_name:
            recording_name = youtube_title
        recording_type = 'youtube'
        logger.info('adding youtube recording, url: %s, id: %s, name: %s',
                    youtube_url, youtube_id, recording_name)
        with RecordingsIndex() as index, TemporaryDirectory(suffix='_ytdownloader', tmp_dir=TMP_BACKEND) as tmp_dir:
            # check if we have a recording in our index
            if index.get_recording(recording_id):
                logger.info('recording already indexed, url: %s, id: %s, name: %s',
                            youtube_url, youtube_id, recording_name)
                return HttpResponse(json.dumps({'status': 'error', 'message': 'Nagranie zostało już zindeksowane'}))
            recording = Recording.of(recording_id, recording_name, recording_type)
            add_youtube_fingerprint(recording, youtube_id, youtube_title, tmp_dir)
            index.add_recording(recording)
        return HttpResponse(json.dumps({'status': 'ok', 'message': 'Dodano nagranie'}))
    except:
        logger.exception('could not add youtube recording')
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Wystąpił błąd'}))

@admin.site.admin_view
def remove_recording(request):
    try:
        recording_id = request.GET['recording_id']
        with RecordingsIndex() as index:
            recording = index.get_recording(recording_id)
            # first remove all fingerprints
            for match_id, _ in recording[RecordingsIndex.MATCHES].items():
                fingerprint.remove_fingerprint.delay(match_id, recording_id).wait()
            # then remove recording from index itself
            index.delete_recording(recording_id)
        return HttpResponse(json.dumps({'status': 'ok', 'message': 'OK'}))
    except:
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Wystąpił błąd'}))

@admin.site.admin_view
def add_youtube_match(request, recording_id):
    try:
        youtube_url = request.GET['youtube_url']
        youtube_id = extract_youtube_id(youtube_url)
        youtube_title = get_youtube_title(youtube_id)
        with RecordingsIndex() as index, TemporaryDirectory(suffix='_ytdownloader', tmp_dir=TMP_BACKEND) as tmp_dir:
            recording = index.get_recording(recording_id)
            logger.info('adding youtube match, url: %s, id: %s, recording: %s',
                        youtube_url, youtube_id, recording)
            add_youtube_fingerprint(recording, youtube_id, youtube_title, tmp_dir)
            index.add_recording(recording)
        return HttpResponse(json.dumps({'status': 'ok', 'message': 'Dodano znacznik'}))
    except:
        logger.exception('could not add youtube match')
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Wystąpił błąd'}))

@admin.site.admin_view
def remove_match(request, recording_id):
    try:
        match_id = request.GET['match_id']
        with RecordingsIndex() as index:
            recording = index.get_recording(recording_id)
            if match_id in recording[RecordingsIndex.MATCHES]:
                logger.info('removing youtube match, match_id: %s, recording: %s', match_id, recording)
                fingerprint.remove_fingerprint.delay(match_id, recording_id).wait()
                del recording[RecordingsIndex.MATCHES][match_id]
                index.add_recording(recording)
        return HttpResponse(json.dumps({'status': 'ok', 'message': 'Usunięto znacznik'}))
    except:
        logger.exception('could not remove youtube match')
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Wystąpił błąd'}))

@admin.site.admin_view
def ignore_match(request, recording_id):
    try:
        match_id = request.GET['match_id']
        with RecordingsIndex() as index:
            recording = index.get_recording(recording_id)
            if match_id in recording[RecordingsIndex.MATCHES]:
                logger.info('ignoring match, match_id: %s, recording: %s', match_id, recording)
                fingerprint.remove_fingerprint.delay(match_id, recording_id).wait()
                recording[RecordingsIndex.MATCHES][match_id][RecordingsIndex.IGNORE] = True
                index.add_recording(recording)
        return HttpResponse(json.dumps({'status': 'ok', 'message': 'Zignorowano znacznik'}))
    except:
        logger.exception('could not ignore match')
        return HttpResponse(json.dumps({'status': 'error', 'message': 'Wystąpił błąd'}))
