'''
Created on 7 kwi 2014

@author: sopel39
'''

from django.shortcuts import render


def index(request, template_name):
    return render(request, template_name)
