'''
Created on 19 sty 2014

@author: sopel39
'''
from django import template
from django.core.urlresolvers import reverse


register = template.Library()

@register.simple_tag
def navactive(request, urls):
    for prefix in (reverse(url) for url in urls.split()):
        if request.path.startswith(prefix):
            return "active"
    return ""
