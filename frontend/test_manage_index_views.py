'''
Created on 17 maj 2014

@author: sopel39
'''
import os
import unittest

from django.contrib.auth.models import User
from django.test.client import RequestFactory
from mock import Mock

import database
from database.recordingsindex import RecordingsIndex, Recording
import manage_index_views


class Test(unittest.TestCase):

    def setUp(self):
        manage_index_views.extract_youtube_id = Mock(return_value='youtube_id')
        manage_index_views.get_youtube_title = Mock(return_value='youtube_title')

        database.youtube.ytdownloader.youtube_download = Mock(return_value='/tmp/mock_path')
        database.youtube.fingerprint.add_fingerprint_to_database = Mock()
        database.youtube.fingerprint.remove_fingerprint = Mock()
        os.remove = Mock()

        # initialize index
        self._index = RecordingsIndex()
        self._index.delete_recording('youtube_youtube_id')

        self.factory = RequestFactory()

    def tearDown(self):
        self._index.close()

    def _get_sample_recording(self, recording_name='recording_name', matches={}):
        return Recording.of('youtube_youtube_id', recording_name, 'youtube', matches=matches)

    def _get_sample_match(self, match_id='match_id', match_name='match_name', ignored=False):
        return {
            match_id: {
                RecordingsIndex.NAME: match_name,
                RecordingsIndex.TYPE: 'youtube',
                RecordingsIndex.URL: 'http://www.youtube.com/watch?v=epNJX7CNK9g',
                RecordingsIndex.IGNORE: ignored
            }
        }

    def _get_staff_user(self):
        user = User()
        user.is_staff = True
        return user

    def test_should_add_youtube_recording(self):
        request = self.factory.get('/frontend_admin/add_youtube_recording', {
                    'youtube_url': 'youtube_url',
                    'recording_name': ''})
        request.user = self._get_staff_user()
        manage_index_views.add_youtube_recording(request)

        recording = self._index.get_recording('youtube_youtube_id')
        self.assertEqual(recording[RecordingsIndex.NAME], 'youtube_title')
        self.assertEqual(len(recording[RecordingsIndex.MATCHES]), 1)
        self.assertIn('youtube_youtube_id', recording[RecordingsIndex.MATCHES])
        self.assert_downloaded('youtube_id')

    def test_should_not_add_indexed_recording(self):
        self._index.add_recording(self._get_sample_recording())

        request = self.factory.get('/frontend_admin/add_youtube_recording', {
                    'youtube_url': 'youtube_url',
                    'recording_name': ''})
        request.user = self._get_staff_user()
        manage_index_views.add_youtube_recording(request)

        recording = self._index.get_recording('youtube_youtube_id')
        self.assertEqual(recording[RecordingsIndex.NAME], 'recording_name')
        self.assertEqual(len(recording[RecordingsIndex.MATCHES]), 0)
        self.assert_not_downloaded()

    def test_should_remove_recording(self):
        self._index.add_recording(self._get_sample_recording())
        self.assertIsNotNone(self._index.get_recording('youtube_youtube_id'))

        request = self.factory.get('/frontend_admin/remove_recording', {'recording_id': 'youtube_youtube_id'})
        request.user = self._get_staff_user()
        manage_index_views.remove_recording(request)

        self.assertIsNone(self._index.get_recording('youtube_youtube_id'))

    def test_should_add_youtube_match(self):
        self._index.add_recording(self._get_sample_recording())
        self.assertEqual(len(self._index.get_recording('youtube_youtube_id')[RecordingsIndex.MATCHES]), 0)

        request = self.factory.get('/frontend_admin/add_youtube_recording', {'youtube_url': 'youtube_url'})
        request.user = self._get_staff_user()
        manage_index_views.add_youtube_match(request, 'youtube_youtube_id')

        recording = self._index.get_recording('youtube_youtube_id')
        self.assertEqual(recording[RecordingsIndex.NAME], 'recording_name')
        self.assertEqual(len(recording[RecordingsIndex.MATCHES]), 1)
        self.assertIn('youtube_youtube_id', recording[RecordingsIndex.MATCHES])
        self.assert_downloaded('youtube_id')

    def test_should_remove_youtube_match(self):
        self._index.add_recording(self._get_sample_recording(matches=self._get_sample_match()))
        self.assertEqual(len(self._index.get_recording('youtube_youtube_id')[RecordingsIndex.MATCHES]), 1)

        request = self.factory.get('/frontend_admin/add_youtube_recording', {'match_id': 'match_id'})
        request.user = self._get_staff_user()
        manage_index_views.remove_match(request, 'youtube_youtube_id')

        self.assert_removed_fingerprint()
        self.assertEqual(len(self._index.get_recording('youtube_youtube_id')[RecordingsIndex.MATCHES]), 0)

    def test_should_ignore_match(self):
        self._index.add_recording(self._get_sample_recording(matches=self._get_sample_match()))

        request = self.factory.get('/frontend_admin/add_youtube_recording', {'match_id': 'match_id'})
        request.user = self._get_staff_user()
        manage_index_views.ignore_match(request, 'youtube_youtube_id')

        match = self._index.get_recording('youtube_youtube_id')[RecordingsIndex.MATCHES]['match_id']
        self.assertTrue(match[RecordingsIndex.IGNORE])
        self.assert_removed_fingerprint()

        manage_index_views.remove_match(request, 'youtube_youtube_id')
        self.assertNotIn('match_id', self._index.get_recording('youtube_youtube_id')[RecordingsIndex.MATCHES])

    def assert_removed_fingerprint(self, match_id='match_id', recording_id='youtube_youtube_id'):
        database.youtube.fingerprint.remove_fingerprint.delay.assert_called_once_with(match_id, recording_id);

    def assert_downloaded(self, youtube_id):
        self.assertEqual(database.youtube.ytdownloader.youtube_download.delay.call_count, 1);
        self.assertEqual(database.youtube.ytdownloader.youtube_download.delay.call_args[0][0], youtube_id)
        self.assertEqual(database.youtube.fingerprint.add_fingerprint_to_database.delay.call_count, 1);

    def assert_not_downloaded(self):
        self.assertFalse(database.youtube.ytdownloader.youtube_download.delay.called)
        self.assertFalse(database.youtube.fingerprint.add_fingerprint_to_database.delay.called)
