'''
Created on 29 cze 2014

@author: sopel39
'''
import base64
import json
import unittest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponse
from django.test.client import RequestFactory
from mock import Mock

import api_views
from database.recordingsindex import RecordingsIndex
import wang


LANDMARKS = 'H4sIAAAAAAAAAGM6y/zi//8Lc86yXGJkSAEAf129mQ4AAAA='

class Test(unittest.TestCase):

    def setUp(self):
        api_views.token_validator.validate_token = Mock()
        wang.tasks.match_fingerprint = Mock()
        wang.tasks.match_fingerprint.delay.return_value.get.return_value = ['match_id']
        wang.tasks.match_landmarks = Mock()
        wang.tasks.match_landmarks.delay.return_value.get.return_value = ['match_id']
        api_views.RecordingsIndex = Mock(spec=RecordingsIndex)
        api_views.RecordingsIndex.return_value.__enter__ = Mock()
        api_views.RecordingsIndex.return_value.__exit__ = Mock()
        api_views.RecordingsIndex.return_value.__enter__.return_value.get_recording_by_match_id.return_value = {
                api_views.RecordingsIndex.NAME: 'recording_name',
                api_views.RecordingsIndex.MATCHES: {
                    'match1': {
                        api_views.RecordingsIndex.NAME: 'match_name',
                        api_views.RecordingsIndex.URL: 'www.youtube.com',
                        api_views.RecordingsIndex.TYPE: 'youtube'
                    }
                }
            }

        self.match_response = HttpResponse(json.dumps({
            "status": "ok",
            "message": "ok",
            "results": [{"url": "www.youtube.com", "type": "youtube", "name": ["recording_name"]}]}),
            content_type="application/json")

        self.factory = RequestFactory()

    def test_should_recognize_using_file(self):
        request = self.factory.post('/api/query_file/', {
                    'token': '',
                    'file': SimpleUploadedFile("file", "")});
        self.assertEqual(str(api_views.query_file(request)), str(self.match_response))

    def test_should_recognize_using_landmarks(self):
        request = self.factory.post('/api/query_landmarks/', {
                    'token': '',
                    'landmarks': SimpleUploadedFile("landmarks", base64.b64decode(LANDMARKS))});
        self.assertEqual(str(api_views.query_landmarks(request)), str(self.match_response))
        wang.tasks.match_landmarks.delay.assert_called_once_with([(1000, 1255, 255, 155), (1234, 1235, 0, 100)])

