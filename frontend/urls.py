from django.conf.urls import patterns, url, include
from django.contrib import admin

import api_views
import home_views
import manage_index_views


admin.autodiscover()
urlpatterns = patterns('',
    url(r'^$', home_views.index, {'template_name': 'index.html'}, name="index"),
    url(r'^api/query_file/$', api_views.query_file, name='api/query_file'),
    url(r'^api/query_landmarks/$', api_views.query_landmarks, name='api/query_landmarks'),
    url(r'^frontend_admin/manage_index/$', manage_index_views.manage_index,
        {'template_name': 'admin/manage_index.html'}, name='admin/manage_index'),
    url(r'^frontend_admin/get_recordings/$', manage_index_views.get_recordings, name='admin/get_recordings'),
    url(r'^frontend_admin/get_matches/(?P<recording_id>[a-zA-Z0-9_\-]+)/$',
        manage_index_views.get_matches, name='admin/get_matches'),
    url(r'^frontend_admin/add_youtube_recording/$', manage_index_views.add_youtube_recording,
        name='admin/add_youtube_recording'),
    url(r'^frontend_admin/remove_recording/$', manage_index_views.remove_recording, name='admin/remove_recording'),
    url(r'^frontend_admin/add_youtube_match/(?P<recording_id>[a-zA-Z0-9_\-]+)/$',
        manage_index_views.add_youtube_match, name='admin/add_youtube_match'),
    url(r'^frontend_admin/remove_match/(?P<recording_id>[a-zA-Z0-9_\-]+)/$',
        manage_index_views.remove_match, name='admin/remove_match'),
    url(r'^frontend_admin/ignore_match/(?P<recording_id>[a-zA-Z0-9_\-]+)/$',
        manage_index_views.ignore_match, name='admin/ignore_match'),
    url(r'^admin/', include(admin.site.urls)),
)
