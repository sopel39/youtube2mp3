# coding=utf-8
'''
Created on 17 sty 2014

@author: sopel39
'''
import StringIO
import json
import logging
import os
import uuid
import zlib

from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
import msgpack

from database.recordingsindex import RecordingsIndex
from settings import frontend as settings
from settings.paths import TMP_CLIENT
from utils import file
from utils.google_token import GoogleTokenValidator, validate_token
from utils.string import str_to_tuple
from utils.temporarydir import TemporaryDirectory
import wang.tasks as fingerprint


GZIP_WBITS = 15 + 32

logger = logging.getLogger(__name__)

token_validator = GoogleTokenValidator(settings.AUDIENCE, settings.CLIENT_IDS, settings.SECRET_TOKEN)

@require_http_methods(["POST"])
@validate_token(token_validator)
def query_file(request):
    try:
        # check file size
        f = request.FILES['file']
        logger.info('query file size is: %d', f.size)
        if f.size > settings.MAX_FILE_SIZE:
            raise RuntimeError('file too big')

        with TemporaryDirectory(suffix='_api', tmp_dir=TMP_CLIENT) as tmp_dir:
            # save uploaded file to tmp directory
            ext = file.extension(f.name)
            tmp_path = os.path.join(tmp_dir, str(uuid.uuid4()) + ext)
            file.store_uploaded_file(f, tmp_path)

            # identify track
            result = fingerprint.match_fingerprint.delay(tmp_path)
            matches = result.get()
            return response_for_matches(matches)
    except:
        logger.exception('query exception')
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Wystąpił błąd'
            }))

@require_http_methods(["POST"])
@validate_token(token_validator)
def query_landmarks(request):
    try:
        # check landmarks size
        gzip_landmarks = request.FILES['landmarks']
        logger.info('compressed landmarks size is: %d', gzip_landmarks.size)
        if gzip_landmarks.size > settings.MAX_LANDMARKS_SIZE:
            raise RuntimeError('landmarks too big')

        # decode landmarks
        msgpack_landmarks = zlib.decompress(gzip_landmarks.read(), GZIP_WBITS)
        unpacker = msgpack.Unpacker(StringIO.StringIO(msgpack_landmarks))
        landmarks = []
        for _ in range(unpacker.next()):
            t1 = unpacker.next()
            t2 = t1 + (unpacker.next() & 0xff)
            f1 = unpacker.next() & 0xff
            f2 = f1 + unpacker.next()
            landmarks.append((t1, t2, f1, f2))

        # identify track
        result = fingerprint.match_landmarks.delay(landmarks)
        matches = result.get()
        return response_for_matches(matches)
    except:
        logger.exception('query exception')
        return HttpResponse(json.dumps({
            'status': 'error',
            'message': u'Wystąpił błąd'
            }))

def response_for_matches(matches):
    # find recordings by match id
    recordings = []
    for match in matches:
        match_id = match[0]
        with RecordingsIndex() as index:
            recording = index.get_recording_by_match_id(match_id)
        if recording:
            for match in recording[RecordingsIndex.MATCHES].values():
                recordings.append({
                    'name': str_to_tuple(recording[RecordingsIndex.NAME]),
                    'url': match[RecordingsIndex.URL],
                    'type': match[RecordingsIndex.TYPE]})
    return HttpResponse(json.dumps({'status': 'ok', 'message': 'ok', 'results': recordings}),
                        content_type="application/json")
