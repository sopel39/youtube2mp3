package com.youtube2mp3.wang;

import static com.youtube2mp3.wang.LandmarkConsts.FFT_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.NFFTS;
import static com.youtube2mp3.wang.LandmarkConsts.N_OFFSETS;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.zip.GZIPOutputStream;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.msgpack.MessagePack;
import org.msgpack.packer.Packer;

import com.youtube2mp3.wang.LandmarksCalculator.Landmark;

public final class LandmarksUtils {

    public static Collection<Landmark> landmarksForFile(String filename, int nOffsets, int maxRows,
            LandmarksCalculatorFactory calculatorFactory) throws Exception {
        Collection<Landmark> landmarks = new ArrayList<Landmark>();
        for (int offset = 0; offset < FFT_SIZE; offset += FFT_SIZE / nOffsets) {
            LandmarksCalculator calculator = calculatorFactory.getNewCalculator(offset, maxRows);
            FrameGrabber grabber = new FFmpegFrameGrabber(filename);
            grabber.start();
            Frame frame;
            while ((frame = grabber.grabFrame()) != null && calculator.pushBuffer((ShortBuffer) frame.samples[0])) {
            }
            grabber.stop();
            landmarks.addAll(calculator.getLandmarks());
        }
        return landmarks;
    }

    public static Collection<Landmark> landmarksForFile(String filename, LandmarksCalculatorFactory calculatorFactory)
            throws Exception {
        return landmarksForFile(filename, N_OFFSETS, NFFTS, calculatorFactory);
    }

    public static byte[] serializeLandmarks(Collection<Landmark> landmarks) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MessagePack msgpack = new MessagePack();
        Packer packer = msgpack.createPacker(new GZIPOutputStream(baos));
        packer.write(landmarks.size());
        for (Landmark landmark : landmarks) {
            packer.write(landmark.t1);
            packer.write((byte) (landmark.t2 - landmark.t1));
            packer.write((byte) landmark.f1);
            packer.write((byte) (landmark.f2 - landmark.f1));
        }
        packer.close();
        return baos.toByteArray();
    }

    private LandmarksUtils() {
    }
}
