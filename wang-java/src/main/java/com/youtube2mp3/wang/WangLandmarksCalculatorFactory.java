package com.youtube2mp3.wang;

import static org.bytedeco.javacv.Parallel.getNumThreads;

public class WangLandmarksCalculatorFactory implements LandmarksCalculatorFactory {

    private final int nThreads;

    public WangLandmarksCalculatorFactory(int nThreads) {
        this.nThreads = nThreads;
    }

    public WangLandmarksCalculatorFactory() {
        this(getNumThreads());
    }

    @Override
    public WangLandmarksCalculator getNewCalculator(int offset, int maxRows) {
        return new WangLandmarksCalculator(offset, maxRows, nThreads);
    }
}
