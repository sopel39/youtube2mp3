package com.youtube2mp3.wang;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.util.Arrays.copyOf;
import static org.bytedeco.javacpp.opencv_core.CV_64FC1;
import static org.bytedeco.javacpp.opencv_core.minMaxIdx;
import static org.bytedeco.javacv.Parallel.loop;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.Parallel.Looper;

public final class MathUtils {

    private int nThreads;
    private int cols;
    private double[] gaussian;

    public MathUtils(int nThreads, int cols, double std) {
        this.nThreads = nThreads;
        this.cols = cols;
        gaussian = gaussian(std, cols * 2 - 1);
    }

    public double sum(final Mat src) {
        final double[] sums = new double[nThreads];
        loop(0, src.rows(), nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                sums[looperID] = opencv_core.sumElems(src.rowRange(from, to)).get();
            }
        });
        return sum(sums);
    }

    public void max(final Mat src, final double val, final Mat dst) {
        loop(0, src.rows(), nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                opencv_core.max(src.rowRange(from, to), val, dst.rowRange(from, to));
            }
        });
    }

    public double amax(final Mat mat) {
        final double[] amaxes = new double[nThreads];
        loop(0, mat.rows(), nThreads, new Looper() {
            @Override
            public void loop(int from, int to, int looperID) {
                double[] amin = new double[1], amax = new double[1];
                minMaxIdx(mat, amin, amax);
                amaxes[looperID] = amax[0];
            }
        });
        return amax(amaxes);
    }

    public void colmax(double[] data, int rows, double[] result) {
        for (int col = 0; col < cols; ++col) {
            double max = NEGATIVE_INFINITY;
            for (int pos = col; pos < rows * cols; pos += cols) {
                max = Math.max(data[pos], max);
            }
            result[col] = max;
        }
    }

    public void log(final Mat src, final Mat dst) {
        loop(0, src.rows(), nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                opencv_core.log(src.rowRange(from, to), dst.rowRange(from, to));
            }
        });
    }

    public void substract(final double[] data, final double scalar) {
        loop(0, data.length, nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                for (int i = from; i < to; ++i) {
                    data[i] -= scalar;
                }
            }
        });
    }

    public void highpass(final double[] data, final double pole) {
        loop(0, cols, nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                for (int col = from; col < to; ++col) {
                    int lastPos = col;
                    double lastX = data[lastPos];
                    for (int currPos = lastPos + cols; currPos < data.length; currPos += cols) {
                        double currX = data[currPos];
                        data[currPos] = currX - lastX + pole * data[lastPos];
                        lastPos = currPos;
                        lastX = currX;
                    }
                }
            }
        });
    }

    public void spread(double[] row, int pos, double val) {
        int gaussianFrom = cols - pos - 1;
        for (int i = 0; i < row.length; ++i) {
            row[i] = Math.max(row[i], gaussian[gaussianFrom + i] * val);
        }
    }

    public int locmax(double[] data, int row, double edge, int[] result) {
        int nResults = 0;
        int lastPositive = -1;
        int from = cols * row;
        double lastValue = edge;
        for (int i = from; i < from + cols; ++i) {
            double currValue = data[i];
            if (lastPositive != -1 && currValue < lastValue) {
                result[nResults++] = lastPositive - from;
                lastPositive = -1;
            } else if (currValue > lastValue) {
                lastPositive = i;
            }
            lastValue = currValue;
        }

        if (lastPositive != -1 && edge < lastValue) {
            result[nResults++] = lastPositive - from;
        }

        return nResults;
    }

    public void spreadLocmax(double[] row, double edge) {
        int[] result = new int[cols];
        int nResults = locmax(row, 0, edge, result);
        double[] origRow = copyOf(row, cols);
        for (int i = 0; i < nResults; ++i) {
            spread(row, result[i], origRow[result[i]]);
        }
    }

    public void copy(double[] data, int row, int[] indices, int length, double[] dst) {
        int from = cols * row;
        for (int i = 0; i < length; ++i) {
            dst[i] = data[from + indices[i]];
        }
    }

    public static double[] gaussian(double std, int size) {
        double[] gaussian = new double[size];
        for (int i = 0; i < size; ++i) {
            double d = i - size / 2;
            gaussian[i] = Math.exp(-d * d / 2. / std / std);
        }
        return gaussian;
    }

    public static Mat hanning(int rows, int size) {
        double[] hanning = new double[size];
        for (int i = 0; i < size; i++) {
            hanning[i] = 0.5 * (1. - cos(2. * PI * i / (size - 1)));
        }

        Mat mat = new Mat(rows, size, CV_64FC1);
        for (int i = 0; i < rows; ++i) {
            mat.getDoubleBuffer(i * size).put(hanning);
        }
        return mat;
    }

    public static double amax(double[] arr) {
        double max = NEGATIVE_INFINITY;
        for (int i = 0; i < arr.length; ++i) {
            max = Math.max(arr[i], max);
        }
        return max;
    }

    public static double sum(double[] arr) {
        double sum = 0.;
        for (int i = 0; i < arr.length; ++i) {
            sum += arr[i];
        }
        return sum;
    }

    public static void mul(double[] arr, double val) {
        for (int i = 0; i < arr.length; ++i) {
            arr[i] *= val;
        }
    }

    // based on:
    // http://stackoverflow.com/questions/951848/java-array-sort-quick-way-to-get-a-sorted-list-of-indices-of-an-array
    public static void quicksortDesc(double[] a, int[] index, int left, int right) {
        if (right <= left)
            return;
        int i = partition(a, index, left, right);
        quicksortDesc(a, index, left, i - 1);
        quicksortDesc(a, index, i + 1, right);
    }

    private static int partition(double[] a, int[] index, int left, int right) {
        int i = left - 1;
        int j = right;
        while (true) {
            while (greater(a[++i], a[right]))
                ;
            while (greater(a[right], a[--j]))
                if (j == left)
                    break;
            if (i >= j)
                break;
            exch(a, index, i, j);
        }
        exch(a, index, i, right);
        return i;
    }

    private static boolean greater(double x, double y) {
        return (x > y);
    }

    private static void exch(double[] a, int[] index, int i, int j) {
        double swap = a[i];
        a[i] = a[j];
        a[j] = swap;
        int b = index[i];
        index[i] = index[j];
        index[j] = b;
    }
}
