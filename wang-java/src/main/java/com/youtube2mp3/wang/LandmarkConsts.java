package com.youtube2mp3.wang;

public class LandmarkConsts {

    public static final int SAMPLE_RATE = 11025;
    public static final int SAMPLE_SECS = 15;
    public static final double DECAY_RATE = 0.988;
    public static final int MAX_PEAKS_PER_FRAME = 5;
    public static final int MAX_PAIRS_PER_PEAK = 3;
    public static final double SPREAD_GAUSSIAN_STD = 30.0;
    public static final double HIGHPASS_POLE = 0.98;
    public static final int TIME_RANGE = 64;
    public static final int BINS_RANGE = 32;
    public static final int TARGET_FFT_SIZE = 512;
    public static final double FFT_TIME = 1000.0 * TARGET_FFT_SIZE / SAMPLE_RATE;
    public static final double HOP_TIME = 23.22;
    public static final int FFT_SIZE = (int) (FFT_TIME * SAMPLE_RATE / 1000.0);
    public static final int FFT_SIZE_HALF = FFT_SIZE / 2;
    public static final int HOP_SIZE = (int) (HOP_TIME * SAMPLE_RATE / 1000.0);
    public static final int FFT_HOP_DIFF = FFT_SIZE - HOP_SIZE;
    public static final int NFFTS = SAMPLE_RATE * SAMPLE_SECS / HOP_SIZE;
    public static final int N_OFFSETS = 4;
}
