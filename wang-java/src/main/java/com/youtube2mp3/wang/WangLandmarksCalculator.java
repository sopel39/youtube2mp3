package com.youtube2mp3.wang;

import static com.youtube2mp3.wang.LandmarkConsts.BINS_RANGE;
import static com.youtube2mp3.wang.LandmarkConsts.DECAY_RATE;
import static com.youtube2mp3.wang.LandmarkConsts.FFT_HOP_DIFF;
import static com.youtube2mp3.wang.LandmarkConsts.FFT_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.FFT_SIZE_HALF;
import static com.youtube2mp3.wang.LandmarkConsts.HIGHPASS_POLE;
import static com.youtube2mp3.wang.LandmarkConsts.HOP_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.MAX_PAIRS_PER_PEAK;
import static com.youtube2mp3.wang.LandmarkConsts.MAX_PEAKS_PER_FRAME;
import static com.youtube2mp3.wang.LandmarkConsts.SPREAD_GAUSSIAN_STD;
import static com.youtube2mp3.wang.LandmarkConsts.TIME_RANGE;
import static com.youtube2mp3.wang.MathUtils.hanning;
import static com.youtube2mp3.wang.MathUtils.mul;
import static com.youtube2mp3.wang.MathUtils.quicksortDesc;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Arrays.fill;
import static org.bytedeco.javacpp.opencv_core.CV_16SC1;
import static org.bytedeco.javacpp.opencv_core.CV_64FC1;
import static org.bytedeco.javacpp.opencv_core.CV_64FC2;
import static org.bytedeco.javacpp.opencv_core.DFT_COMPLEX_OUTPUT;
import static org.bytedeco.javacpp.opencv_core.DFT_ROWS;
import static org.bytedeco.javacpp.opencv_core.dft;
import static org.bytedeco.javacpp.opencv_core.magnitude;
import static org.bytedeco.javacpp.opencv_core.multiply;
import static org.bytedeco.javacpp.opencv_core.split;
import static org.bytedeco.javacv.Parallel.loop;

import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacv.Parallel.Looper;

public class WangLandmarksCalculator implements LandmarksCalculator {

    private static final int DFT_CHUNK_ROWS = 10;
    private static final int DFT_CHUNK_SAMPLES = DFT_CHUNK_ROWS * HOP_SIZE + FFT_HOP_DIFF;
    private static final int INIT_THRESHOLD_ROWS = 10;

    private int nThreads;
    private MathUtils math;

    private Mat samples;
    private ShortBuffer samplesShortBuffer;
    private int offset;
    private int processedSamples;

    private Mat hanning;
    private LooperTaskQueue dftTaskQueue;

    private Mat matDataBuffer;
    private int totalRows;
    private double[] data;

    private int[][] peakBinsIn;
    private int[] nPeakBinsIn;

    private int[][] peakBinsOut;
    private int[] nPeakBinsOut;

    public WangLandmarksCalculator(int offset, int maxRows, int nThreads) {
        this.nThreads = nThreads;
        math = new MathUtils(nThreads, FFT_SIZE_HALF, SPREAD_GAUSSIAN_STD);

        samples = new Mat(1, maxRows * HOP_SIZE + FFT_HOP_DIFF, CV_16SC1);
        samplesShortBuffer = samples.getShortBuffer();
        this.offset = offset;

        hanning = hanning(DFT_CHUNK_ROWS, FFT_SIZE);
        dftTaskQueue = new LooperTaskQueue(getDFTLoopers());

        matDataBuffer = new Mat(maxRows, FFT_SIZE_HALF, CV_64FC1);
    }

    @Override
    public boolean pushBuffer(ShortBuffer buffer) {
        int shortBufferLimit = buffer.limit();
        int toCopy = min(buffer.remaining(), samplesShortBuffer.remaining());
        buffer.limit(buffer.position() + toCopy);
        samplesShortBuffer.put(buffer);
        buffer.limit(shortBufferLimit);
        while (samplesShortBuffer.position() - processedSamples - offset >= DFT_CHUNK_SAMPLES) {
            processDFTSamples();
        }
        return samplesShortBuffer.hasRemaining();
    }

    private class DFTLooper implements Looper {
        Mat complex;
        Mat real;
        Mat img;
        MatVector complexVector;

        DFTLooper() {
            complex = new Mat(DFT_CHUNK_ROWS, FFT_SIZE, CV_64FC2);
            real = new Mat(DFT_CHUNK_ROWS, FFT_SIZE, CV_64FC1);
            img = new Mat(DFT_CHUNK_ROWS, FFT_SIZE, CV_64FC1);
            complexVector = new MatVector(real, img);
        }

        @Override
        public void loop(int rowFrom, int rowTo, int looperID) {
            for (int row = rowFrom; row < rowTo; ++row) {
                int samplesStart = row * HOP_SIZE + offset;
                samples.colRange(samplesStart, samplesStart + FFT_SIZE).convertTo(real.row(row - rowFrom), CV_64FC1);
            }
            multiply(real, hanning, real);
            dft(real, complex, DFT_COMPLEX_OUTPUT | DFT_ROWS, 0);
            split(complex, complexVector);

            Mat magnitudes = real.colRange(0, FFT_SIZE_HALF);
            magnitude(magnitudes, img.colRange(0, FFT_SIZE_HALF), magnitudes);
            magnitudes.rowRange(0, rowTo - rowFrom).copyTo(matDataBuffer.rowRange(rowFrom, rowTo));
        }
    }

    private List<Looper> getDFTLoopers() {
        List<Looper> loopers = new ArrayList<Looper>(nThreads);
        for (int i = 0; i < nThreads; ++i) {
            loopers.add(new DFTLooper());
        }
        return loopers;
    }

    private void processDFTSamples() {
        int nRows = (samplesShortBuffer.position() - FFT_HOP_DIFF - processedSamples - offset) / HOP_SIZE;
        nRows = max(min(nRows, DFT_CHUNK_ROWS), 0);
        dftTaskQueue.loop(totalRows, totalRows + nRows);
        totalRows += nRows;
        processedSamples += nRows * HOP_SIZE;
    }

    @Override
    public Collection<Landmark> getLandmarks() {
        processDFTSamples();
        shutdownAndWaitDftTaskQueue();
        dftTaskQueue = null;
        samples = null;
        samplesShortBuffer = null;
        hanning = null;

        if (totalRows == 0) {
            return new ArrayList<Landmark>();
        }

        final Mat matData = matDataBuffer.rowRange(0, totalRows);
        double amax = math.amax(matData);
        math.max(matData, amax / 1e6, matData);
        math.log(matData, matData);
        double mean = math.sum(matData) / matData.rows() / matData.cols();

        data = new double[totalRows * FFT_SIZE_HALF];
        matData.getDoubleBuffer().get(data, 0, totalRows * FFT_SIZE_HALF);
        matDataBuffer = null;

        math.substract(data, mean);
        math.highpass(data, HIGHPASS_POLE);

        double[] threshold = new double[FFT_SIZE_HALF];
        math.colmax(data, INIT_THRESHOLD_ROWS, threshold);
        math.spreadLocmax(threshold, 0.);

        peakBinsIn = new int[totalRows][FFT_SIZE_HALF];
        nPeakBinsIn = new int[totalRows];
        peakBinsOut = new int[totalRows][FFT_SIZE_HALF];
        nPeakBinsOut = new int[totalRows];
        loop(0, totalRows, nThreads, new LocmaxPeakBinsLooper());
        forwardFindPeaks(threshold);

        int[][] peakBins = peakBinsIn;
        int[] nPeakBins = nPeakBinsIn;
        peakBinsIn = peakBinsOut;
        nPeakBinsIn = nPeakBinsOut;
        peakBinsOut = peakBins;
        nPeakBinsOut = nPeakBins;
        fill(threshold, 0.);
        backwardFindPeaks(threshold);

        return peaksToLandmarks();
    }

    private void shutdownAndWaitDftTaskQueue() {
        try {
            dftTaskQueue.shutdownAndWait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private class LocmaxPeakBinsLooper implements Looper {
        double[][] amplitudes = new double[nThreads][FFT_SIZE_HALF];

        @Override
        public void loop(int from, int to, int looperID) {
            double[] loopAmplitudes = amplitudes[looperID];
            for (int row = from; row < to; ++row) {
                int[] rowPeakBins = peakBinsIn[row];
                int nRowPeakBins = nPeakBinsIn[row] = math.locmax(data, row, 0., rowPeakBins);
                math.copy(data, row, rowPeakBins, nRowPeakBins, loopAmplitudes);
                quicksortDesc(loopAmplitudes, rowPeakBins, 0, nRowPeakBins - 1);
            }
        }
    }

    private void forwardFindPeaks(double[] threshold) {
        for (int row = 0; row < totalRows; ++row) {
            findRowPeaks(threshold, row);
        }
    }

    private void backwardFindPeaks(double[] threshold) {
        for (int row = totalRows - 1; row >= 0; --row) {
            findRowPeaks(threshold, row);
        }
    }

    private void findRowPeaks(double[] threshold, int row) {
        int[] rowPeakBinsIn = peakBinsIn[row];
        int nRowPeakBinsIn = nPeakBinsIn[row];
        int[] rowPeakBinsOut = peakBinsOut[row];
        int addedPeaks = 0;
        int rowFrom = FFT_SIZE_HALF * row;
        for (int i = 0; i < nRowPeakBinsIn && addedPeaks < MAX_PEAKS_PER_FRAME; ++i) {
            int peakBin = rowPeakBinsIn[i];
            double peakAmpl = data[rowFrom + peakBin];
            if (peakAmpl <= threshold[peakBin]) {
                continue;
            }
            rowPeakBinsOut[addedPeaks++] = peakBin;
            math.spread(threshold, peakBin, peakAmpl);
        }
        nPeakBinsOut[row] = addedPeaks;
        mul(threshold, DECAY_RATE);
    }

    private void findPairs(int t1, int t1PeakBin, Collection<Landmark> landmarks) {
        int nPairs = 0;
        for (int t2 = t1 + 1; t2 < min(t1 + 1 + TIME_RANGE, totalRows); ++t2) {
            int[] t2PeakBins = peakBinsOut[t2];
            for (int i = nPeakBinsOut[t2] - 1; i >= 0; --i) {
                int t2PeakBin = t2PeakBins[i];
                if (abs(t1PeakBin - t2PeakBin) < BINS_RANGE) {
                    landmarks.add(new Landmark(t1, t2, t1PeakBin, t2PeakBin));
                    if (++nPairs >= MAX_PAIRS_PER_PEAK) {
                        return;
                    }
                }
            }
        }
    }

    private Collection<Landmark> peaksToLandmarks() {
        final Collection<Landmark> landmarks = new ConcurrentLinkedQueue<Landmark>();
        loop(0, totalRows, nThreads, new Looper() {
            public void loop(int from, int to, int looperID) {
                for (int t1 = from; t1 < to; ++t1) {
                    int[] t1PeakBins = peakBinsOut[t1];
                    for (int i = nPeakBinsOut[t1] - 1; i >= 0; --i) {
                        int t1PeakBin = t1PeakBins[i];
                        findPairs(t1, t1PeakBin, landmarks);
                    }
                }
            }
        });
        return landmarks;
    }
}
