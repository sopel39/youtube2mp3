package com.youtube2mp3.wang;

import static java.util.concurrent.Executors.newFixedThreadPool;

import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.bytedeco.javacv.Parallel.Looper;

public class LooperTaskQueue {

    private ExecutorService executorService;
    private Queue<Looper> loopers;

    public LooperTaskQueue(Collection<Looper> loopers) {
        executorService = newFixedThreadPool(loopers.size());
        this.loopers = new ConcurrentLinkedQueue<Looper>(loopers);
    }

    public void loop(final int from, final int to) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Looper looper = loopers.poll();
                looper.loop(from, to, 0);
                loopers.add(looper);
            }
        });
    }

    public void shutdownAndWait() throws InterruptedException {
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }
}
