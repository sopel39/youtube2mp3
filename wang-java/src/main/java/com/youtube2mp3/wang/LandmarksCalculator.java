package com.youtube2mp3.wang;

import java.nio.ShortBuffer;
import java.util.Collection;

public interface LandmarksCalculator {

    class Landmark {
        public final int t1;
        public final int t2;
        public final int f1;
        public final int f2;

        public Landmark(int t1, int t2, int f1, int f2) {
            this.t1 = t1;
            this.t2 = t2;
            this.f1 = f1;
            this.f2 = f2;
        }

        @Override
        public String toString() {
            return "Landmark [t1=" + t1 + ", t2=" + t2 + ", f1=" + f1 + ", f2=" + f2 + "]";
        }
    }

    boolean pushBuffer(ShortBuffer buffer);

    Collection<Landmark> getLandmarks();
}
