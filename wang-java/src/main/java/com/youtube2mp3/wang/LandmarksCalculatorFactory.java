package com.youtube2mp3.wang;

public interface LandmarksCalculatorFactory {

    LandmarksCalculator getNewCalculator(int offset, int maxRows);
}
