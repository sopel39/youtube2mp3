package com.youtube2mp3.wang;

import static com.youtube2mp3.wang.LandmarkConsts.NFFTS;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.zip.GZIPInputStream;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;
import org.junit.Test;
import org.msgpack.MessagePack;
import org.msgpack.unpacker.Unpacker;

import com.youtube2mp3.wang.LandmarksCalculator.Landmark;

public class WangTest {

    private static final String AUDIO_INPUT_WAV = "../data/audios/bugs.wav";
    private static final String AUDIO_INPUT_MP3 = "../data/audios/bugs-full.mp3";
    private static final String AUDIO_OUTPUT_WAV = "out.wav";

    static {
        Loader.load(opencv_core.class);
    }

    @Test
    public void testConvert() throws Exception {
        FrameGrabber grabber = new FFmpegFrameGrabber(AUDIO_INPUT_MP3);
        grabber.start();

        FrameRecorder recorder = new FFmpegFrameRecorder(AUDIO_OUTPUT_WAV, 1);
        recorder.setSampleRate(8000);
        recorder.setFormat("wav");
        recorder.start();

        Frame frame;
        while ((frame = grabber.grabFrame()) != null) {
            recorder.record(frame);
        }

        recorder.stop();
        grabber.stop();
    }

    @Test
    public void testLandmarks() throws Exception {
        FrameGrabber grabber = new FFmpegFrameGrabber(AUDIO_INPUT_WAV);
        grabber.start();
        LandmarksCalculatorFactory calculatorFactory = new WangLandmarksCalculatorFactory(1);
        LandmarksCalculator calculator = calculatorFactory.getNewCalculator(256, 11024 * 60 / 256);

        Frame frame;
        while ((frame = grabber.grabFrame()) != null && calculator.pushBuffer((ShortBuffer) frame.samples[0])) {
        }

        Collection<Landmark> marks = calculator.getLandmarks();
        System.out.println(marks);
        System.out.println(marks.size());
    }

    @Test
    public void testLandmarksForFile() throws Exception {
        LandmarksCalculatorFactory calculatorFactory = new WangLandmarksCalculatorFactory(1);
        Collection<Landmark> marks = LandmarksUtils.landmarksForFile(AUDIO_INPUT_WAV, 4, 11024 * 60 / 256,
                calculatorFactory);
        System.out.println(marks);
        System.out.println(marks.size());
        System.out.println(printBase64Binary(LandmarksUtils.serializeLandmarks(marks)));
    }

    @Test
    public void testEmptyLandmarks() throws Exception {
        LandmarksCalculatorFactory calculatorFactory = new WangLandmarksCalculatorFactory(1);
        LandmarksCalculator calculator = calculatorFactory.getNewCalculator(0, NFFTS);
        assertThat(calculator.getLandmarks().isEmpty(), is(true));
    }

    @Test
    public void testSerializeLandmarks() throws Exception {
        Landmark landmark1 = new Landmark(1000, 1255, 255, 155);
        Landmark landmark2 = new Landmark(1234, 1235, 0, 100);
        byte[] bytes = LandmarksUtils.serializeLandmarks(Arrays.asList(landmark1, landmark2));
        System.out.println(printBase64Binary(bytes));
        MessagePack msgpack = new MessagePack();
        Unpacker unpacker = msgpack.createUnpacker(new GZIPInputStream(new ByteArrayInputStream(bytes)));
        assertThat(unpacker.readInt(), is(2));
        assertThat(unpacker.readInt(), is(1000));
        assertThat(unpacker.readByte() & 0xff, is(255));
        assertThat(unpacker.readByte() & 0xff, is(255));
        assertThat((int) unpacker.readByte(), is(-100));
        assertThat(unpacker.readInt(), is(1234));
        assertThat(unpacker.readByte() & 0xff, is(1));
        assertThat(unpacker.readByte() & 0xff, is(0));
        assertThat((int) unpacker.readByte(), is(100));
    }
}
