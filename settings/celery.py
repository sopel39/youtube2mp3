'''
Created on 12 sty 2014

@author: sopel39
'''
from kombu.common import Broadcast


BROKER_URL = 'amqp://'
CELERY_RESULT_BACKEND = 'amqp://'
AUDFPRINT_WATCHER_QUEUE = 'audfprint_watcher'
AUDFPRINT_MATCHER_QUEUE = 'audfprint_matcher'
AUDFPRINT_MATCHER_BROADCAST = 'audfprint_matcher_broadcast'
WANG_WATCHER_QUEUE = 'wang_watcher'
WANG_MATCHER_QUEUE = 'wang_matcher'
CELERY_QUEUES = (Broadcast(AUDFPRINT_MATCHER_BROADCAST),)
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
# use custom worker logging
CELERYD_HIJACK_ROOT_LOGGER = False
# rotate workers so system memory is cleaned
CELERYD_MAX_TASKS_PER_CHILD = 100
