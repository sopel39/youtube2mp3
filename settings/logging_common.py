'''
Created on 26 kwi 2014

@author: sopel39
'''

import os


CURR_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(CURR_DIR, '..', 'logs')

def get_basic_configuration(log_filename, console=False, email=True, root_level='INFO', email_level='ERROR'):
    basic_configuration = {
        'version': 1,
        'disable_existing_loggers': False
    }
    # add simple formatter
    basic_configuration['formatters'] = {
        'simple': {
            'format': '[%(asctime)s %(levelname)s/%(name)s] %(message)s',
            'datefmt': '%y.%m.%d, %H:%M:%S',
        },
    }
    # add standard file logger
    basic_configuration['handlers'] = {
        'file': {
            'level': 'DEBUG',
            'class': 'cloghandler.ConcurrentRotatingFileHandler',
            'filename': os.path.join(LOG_DIR, log_filename),
            'formatter': 'simple',
            'maxBytes': 1024 * 1024 * 50,  # 50 mb
            'backupCount': 5
        },
    }
    handlers = ['file']
    # add optional email logger
    if email:
        basic_configuration['handlers']['email'] = {
            'level': email_level,
            'class':'logging.handlers.SMTPHandler',
            'mailhost': 'localhost',
            'fromaddr': 'log@jakapiosenka.pl',
            'toaddrs': ['karol.sobczak@karolsobczak.com'],
            'subject': 'System log',
        }
        handlers.append('email')
    # add optional console logger
    if console:
        basic_configuration['handlers']['console'] = {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
        handlers.append('console')
    # setup root logger
    basic_configuration['loggers'] = {
        '': {
            'handlers': handlers,
            'level': root_level,
        },
    }
    return basic_configuration
