'''
Created on 5 lut 2014

@author: sopel39
'''

SAMPLE_RATE = 11025
# decides on number of hashes per second
DECAY_RATE = 0.988
MAX_PEAKS_PER_FRAME = 5
MAX_PAIRS_PER_PEAK = 3
SPREAD_GAUSSIAN_STD = 30.0
HIGHPASS_POLE = 0.98
# how far ahead we are looking for pairs
TIME_RANGE = 64
# how many surrounding bins in each direction we
# take into account when looking for pairs
BINS_RANGE = 32
DTIME_BITS = 6
DBINS_BITS = 6
TARGET_FFT_SIZE = 512
# FFT and hop time in milliseconds
FFT_TIME = 1000.0 * TARGET_FFT_SIZE / SAMPLE_RATE
HOP_TIME = 23.22
FFT_SIZE = int(FFT_TIME * SAMPLE_RATE / 1000)
HOP_SIZE = int(HOP_TIME * SAMPLE_RATE / 1000)
# number of top results returned from a database
N_TOP_RESULTS = 100
MATCH_HITS_THRESHOLD = 10
# match ratio threshold between first and the second match
MATCH_RATIO_THRESHOLD = 1.0
# part size is 30 secs and hop size is 15 secs
PART_SIZE = SAMPLE_RATE * 30
PART_HOP = SAMPLE_RATE * 15
