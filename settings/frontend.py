'''
Created on 12 kwi 2014

@author: sopel39
'''

import os


CURR_DIR = os.path.dirname(os.path.abspath(__file__))

AUDIENCE = '677122513805.apps.googleusercontent.com'
CLIENT_IDS = ['677122513805-90a9a6ltjlss2moiuqvbs97701nivivb.apps.googleusercontent.com']
MAX_FILE_SIZE = 10000000
MAX_LANDMARKS_SIZE = 100000

SECRET_TOKEN_PATH = os.path.join(CURR_DIR, 'secret_token.txt')
if os.path.isfile(SECRET_TOKEN_PATH):
    with open(SECRET_TOKEN_PATH) as f:
        SECRET_TOKEN = f.read().strip()
else:
    SECRET_TOKEN = None
