'''
Created on 26 sty 2014

@author: sopel39
'''

from datetime import date


DB_CONN_STRING = "host='localhost' dbname='searchbot' user='searchbot' password='searchbot'"

COUNTRY = '(PL OR US)'

# full brainz scan parameters
FULL_SCAN_END_DATE = date(1970, 1, 1)
FULL_SCAN_DAYS_THRESHOLD = 0

# daily brainz scan parameters
DAILY_SCAN_DAYS_RANGE = 60
DAILY_SCAN_DAYS_THRESHOLD = 0
DAILY_SCAN_HOUR = 1

BRAINZ_URL = 'http://musicbrainz.org/ws/2/recording'
BRAINZ_NAMESPACE = 'http://musicbrainz.org/ns/mmd-2.0#'

# brainz - youtube match parameters
MATCH_WORDS_THRESHOLD = 0.7
MATCH_LENGTHS_THRESHOLD = 0.9

YOUTUBE_MATCH_WORKERS = 1
COOLDOWN_SLEEP_TIME = 3

YTDOWNLOADER_TIMEOUT = 120
