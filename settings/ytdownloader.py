'''
Created on 29 sty 2014

@author: sopel39
'''

import os


CURR_DIR = os.path.dirname(os.path.abspath(__file__))
RELATIVE_YOUTUBEDL_DIR = os.path.join(CURR_DIR, '..', 'bin')
YOUTUBEDL_DIR = os.getenv('YOUTUBEDL_DIR', RELATIVE_YOUTUBEDL_DIR)
YOUTUBEDL_BIN = os.path.join(YOUTUBEDL_DIR, 'youtube-dl')

NUMBER_OF_WORKERS = 1
TIMEOUT = 120