'''
Created on 7 sty 2014

@author: sopel39
'''

import os


CURR_DIR = os.path.dirname(os.path.abspath(__file__))
RELATIVE_AUDFPRINT_DIR = os.path.join(CURR_DIR, '..', 'bin')
AUDFPRINT_DIR = os.getenv('AUDFPRINT_DIR', RELATIVE_AUDFPRINT_DIR)
AUDFPRINT_BIN = os.path.join(AUDFPRINT_DIR, 'audfprint')
RELATIVE_DBASE_DIR = os.path.join(CURR_DIR, '..', 'dbase', 'audfprint')
DBASE = os.getenv('AUDFPRINT_DBASE', os.path.join(RELATIVE_DBASE_DIR, 'dbase.mat'))

ADDCHECKPOINT = 50
DENSITY = 30
TARGERSR = 11025
MATCHMINCOUNT = 1

TIMEOUT = 120
