'''
Created on 5 lut 2014

@author: sopel39
'''

from scipy import signal
import scipy


def stft(data, window, fft_size, hop_size):
    return scipy.array([scipy.fft(window * data[i:i + fft_size])[:(fft_size + 1) / 2]
                        for i in range(0, len(data) - fft_size + 1, hop_size)])

def gaussian(pos, std, size):
    return signal.gaussian(size * 2 - 1, std)[size - pos - 1: 2 * size - pos - 1]

def locmax(a, edge=None):
    if edge != None:
        a = scipy.concatenate(([edge], a, [edge]))
    grad = scipy.diff(a)
    extr_ind = grad.nonzero()[0]
    extr = grad[extr_ind]
    return extr_ind[((extr[:-1] > 0) & (extr[1:] < 0)).nonzero()[0] + (0 if edge != None else 1)]

def spread(a, pos, val, std):
    return scipy.maximum(a, gaussian(pos, std, len(a)) * val)
