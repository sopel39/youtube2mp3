'''
Created on 6 kwi 2014

@author: sopel39
'''

import json
import logging
import ssl
import time

from Crypto.PublicKey import RSA
from django.http import HttpResponseForbidden
import jwt
from pyasn1.codec.der import decoder, encoder
from pyasn1_modules import rfc2459
import requests


logger = logging.getLogger(__name__)

GOOGLE_CERTS_URI = 'https://www.googleapis.com/oauth2/v1/certs'
UTCTIME_FORMAT = '%y%m%d%H%M%SZ'
DECODED_SECRET_TOKEN = 'secret_token'

class GoogleTokenValidator(object):
    def __init__(self, audience, client_ids, secret_token=None):
        self._audience = audience
        self._client_ids = client_ids
        self._certificates = None
        self._secret_token = secret_token

    def _decode_certificate(self, pem_certificate):
        return decoder.decode(ssl.PEM_cert_to_DER_cert(pem_certificate), asn1Spec=rfc2459.Certificate())[0]

    def _decode_certificates(self, pem_certificates):
        return {key: self._decode_certificate(pem_certificate) for key, pem_certificate in pem_certificates.items()}

    def _get_valid_to(self, certificate):
        return time.strptime(str(certificate['tbsCertificate']['validity']['notAfter']['utcTime']), UTCTIME_FORMAT)

    def _get_public_key(self, certificate):
        # extract public key from Google certificate
        return RSA.importKey(encoder.encode(certificate['tbsCertificate']['subjectPublicKeyInfo']))

    def _are_certificates_expired(self, check_expired=True):
        # check if all certificates all up to date
        certificates = self._certificates
        now = time.gmtime()
        return (True if not certificates else
                any([self._get_valid_to(certificate) < now and check_expired for certificate in certificates.values()]))

    def _get_certificates(self, check_expired):
        if self._are_certificates_expired(check_expired):
            logger.info('updating Google certificates')
            # get the newest Google certificates
            response = requests.get(GOOGLE_CERTS_URI)
            response.raise_for_status()
            pem_certificates = json.loads(response.content)
            self._certificates = self._decode_certificates(pem_certificates)
        return self._certificates

    def validate_token(self, token, verify=True, verify_expiration=True, check_expired=True):
        if self._secret_token and token == self._secret_token:
            return DECODED_SECRET_TOKEN

        certificates = self._get_certificates(check_expired)
        for certificate in certificates.values():
            public_key = self._get_public_key(certificate)
            try:
                decoded_token = jwt.decode(token, key=public_key, verify=verify, verify_expiration=verify_expiration)
                if decoded_token['aud'] == self._audience and decoded_token['azp'] in self._client_ids:
                    return decoded_token
            except:
                pass
        # token could not be verified by any certificate
        return None

def validate_token(token_validator):
    def wrap(func):
        def validate_and_call(request, *args, **kwargs):
            if not token_validator.validate_token(request.POST['token']):
                return HttpResponseForbidden()
            return func(request, *args, **kwargs)
        return validate_and_call
    return wrap

