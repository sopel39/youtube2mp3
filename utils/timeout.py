'''
Created on 11 sty 2014

@author: sopel39
'''

import signal


class TimeoutError(Exception):
    pass

def timeout(timeout_time):
    def timeout_function(f):
        def f2(*args):
            def timeout_handler(signum, frame):
                raise TimeoutError()

            old_handler = signal.signal(signal.SIGALRM, timeout_handler)
            signal.alarm(timeout_time)  # triger alarm in timeout_time seconds
            try:
                return f(*args)
            finally:
                signal.signal(signal.SIGALRM, old_handler)
                signal.alarm(0)
        return f2
    return timeout_function
