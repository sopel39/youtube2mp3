# -*- coding: utf-8 -*-
'''
Created on 27 sty 2014

@author: sopel39
'''

import itertools
import re


def maketrans(src, dst):
    if len(src) != len(dst):
        raise ValueError("'from' and 'to' must be equal in length.")
    return dict((ord(a), b) for a, b in itertools.izip(src, dst))

def pl_to_eng(s):
    pl_to_eng_tab = maketrans(u'ąćęłńóśżźĄĆĘŁŃÓŚŻŹ', u'acelnoszzACELNOSZZ')
    return s.translate(pl_to_eng_tab) if isinstance(s, unicode) else s

def cleanup(s):
    return re.sub(r'[^a-zA-Z0-9 ]+', '', pl_to_eng(s))

def remove_parenthesis(s):
    return re.sub('\[.+?\]\s*', '', re.sub(r'\(.+?\)\s*', '', s))

def time_to_millis(s):
    hours, minutes, seconds = (["0", "0"] + s.split(":"))[-3:]
    hours = int(hours)
    minutes = int(minutes)
    seconds = float(seconds)
    return int(3600000 * hours + 60000 * minutes + 1000 * seconds)

def tuple_to_str(t):
    return ' & '.join([unescaped.replace('&', ' && ') for unescaped in t])

def str_to_tuple(s):
    return tuple([escaped.replace(' && ', '&') for escaped in s.split(' & ')])
