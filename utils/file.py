'''
Created on 11 sty 2014

@author: sopel39
'''

import logging
import os
import shutil
import time


logger = logging.getLogger('utils')

def extension(path):
    return os.path.splitext(path)[1]

def basename_no_suffix(path):
    return os.path.splitext(os.path.basename(path))[0]

def force_remove(path):
    try:
        os.remove(path)
    except:
        pass

def clean_dir(dirname):
    [os.remove(os.path.join(dirname, filename)) for filename in os.listdir(dirname)]

def wait_file(path):
    while not os.path.exists(path):
        time.sleep(0.1)
    time.sleep(0.1)

def movefile(src, dst):
    if os.path.exists(dst):
        raise IOError, "Destination path '%s' already exists" % dst
    shutil.move(src, dst)
    return dst

def match_line_start(f, line_start):
    for line in iter(f.readline, ''):
        print line
        logger.debug('match_line_start: %s', line)
        if line.startswith(line_start):
            return
    raise RuntimeError('Could not match line start: ' + line_start)

def store_uploaded_file(f, path):
    with open(path, 'w') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
