'''
Created on 6 kwi 2014

@author: sopel39
'''
import unittest

from google_token import GoogleTokenValidator, DECODED_SECRET_TOKEN


TOKEN = 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImU2ZDAwMTA3MGNiZDQ4ZDY2YTY4ZTNlOWJlODc3NTVlODlmMWEwZWQifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiaWQiOiIxMTY4NjIxOTM4NTIxODg5MTQwMjIiLCJzdWIiOiIxMTY4NjIxOTM4NTIxODg5MTQwMjIiLCJhenAiOiI2NzcxMjI1MTM4MDUtOTBhOWE2bHRqbHNzMm1vaXVxdmJzOTc3MDFuaXZpdmIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJlbWFpbCI6Im5hcGV3bm90cmFmaUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXVkIjoiNjc3MTIyNTEzODA1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwidmVyaWZpZWRfZW1haWwiOnRydWUsImNpZCI6IjY3NzEyMjUxMzgwNS05MGE5YTZsdGpsc3MybW9pdXF2YnM5NzcwMW5pdml2Yi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImlhdCI6MTM5NjgwNDMzMywiZXhwIjoxMzk2ODA4MjMzfQ.VBBeT7hbcvDBEHQbyL-9TVHoRsDEby4KTdPzCX2LbW0Iiip7vjfPjLgqKs5Dd-tGfbUcxFZvZyXoWLpnoFMc7lixhv3WcfynuTQCiItI3ZH97PhBFgZXzH0GZ3F_qpXuzStGrwYEBq22vEqvHXpbIsgdjpFMH6a8GIcXzsm_JXY'

AUDIENCE = '677122513805.apps.googleusercontent.com'
CLIENT_IDS = ['677122513805-90a9a6ltjlss2moiuqvbs97701nivivb.apps.googleusercontent.com']

OLD_GOOGLE_CERTS = {
 "275ff98500b854efc58ea22d984080ff5c6c0f74": "-----BEGIN CERTIFICATE-----\nMIICITCCAYqgAwIBAgIIamfv20OObcUwDQYJKoZIhvcNAQEFBQAwNjE0MDIGA1UE\nAxMrZmVkZXJhdGVkLXNpZ25vbi5zeXN0ZW0uZ3NlcnZpY2VhY2NvdW50LmNvbTAe\nFw0xNDA0MDQyMzQzMzRaFw0xNDA0MDYxMjQzMzRaMDYxNDAyBgNVBAMTK2ZlZGVy\nYXRlZC1zaWdub24uc3lzdGVtLmdzZXJ2aWNlYWNjb3VudC5jb20wgZ8wDQYJKoZI\nhvcNAQEBBQADgY0AMIGJAoGBAMb8C7VMzOIFijV/bQhK0MUGdYokvlGE+fpDeL1O\nHQIZAVwW9nlKVQrMsT7k3heQ4n4znruYjdQNnzIxPdlXnkC9gSbCVSGHM7jRKxKd\nvNz8qvvsiInqiE6KyauoTyJmsNGckv7B4ZZCbNHc+h/I5/h0LjdCkjEEXDjk/3Ge\nBVApAgMBAAGjODA2MAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgeAMBYGA1Ud\nJQEB/wQMMAoGCCsGAQUFBwMCMA0GCSqGSIb3DQEBBQUAA4GBACDuY7ossLDitm0i\nlPxdkzdu7a/ARF0YmLJ2BAlUQMcxiiT8gi/Sig6ta4ReUzCMeUgXg8DOOnccQGWw\nG1g5MmaIBE/Vn3mTGXo7xxKLBi9e1G81yaad8NyWXwt7VCimZpjnPtK7MOM1Pmvx\nuU6pRZDges3dIctF8zBwZswBrQBY\n-----END CERTIFICATE-----\n",
 "e6d001070cbd48d66a68e3e9be87755e89f1a0ed": "-----BEGIN CERTIFICATE-----\nMIICITCCAYqgAwIBAgIIF4ArgnobtLEwDQYJKoZIhvcNAQEFBQAwNjE0MDIGA1UE\nAxMrZmVkZXJhdGVkLXNpZ25vbi5zeXN0ZW0uZ3NlcnZpY2VhY2NvdW50LmNvbTAe\nFw0xNDA0MDUyMzI4MzRaFw0xNDA0MDcxMjI4MzRaMDYxNDAyBgNVBAMTK2ZlZGVy\nYXRlZC1zaWdub24uc3lzdGVtLmdzZXJ2aWNlYWNjb3VudC5jb20wgZ8wDQYJKoZI\nhvcNAQEBBQADgY0AMIGJAoGBAJOG9gFJufSQaJMfdolQT5lgQfmS2R/gAysk8mKU\nzAjjt2U+FJr3n1KIGlTG0q6+Ql2whQ/Dwln0F4LtDI2PJarbkRNVl5Knq5zySN9i\nGayqG3SIHi9C7GMTs6EklO9t8v24eKbUv0HrlKUfaQef5AbXSMIOzsaXUKp4rsRj\nPbQBAgMBAAGjODA2MAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgeAMBYGA1Ud\nJQEB/wQMMAoGCCsGAQUFBwMCMA0GCSqGSIb3DQEBBQUAA4GBAIYrKvEEnndGg/pV\nzy0hr0+C/3i3vv9IrR+jkCdk0KJ60SD7gfFBa34uniY8Bskko5Vls5suS4ETDULL\nXNq/4ipJPzDrGEqfIq5RfoauzehI9cWCa5KjtzcbVB1sCNjJyV3U1Itu8o5pPMBd\nlCa2WFIXfv0DMQ+ROYn8KXvt/B14\n-----END CERTIFICATE-----\n"
}

INVALID_CERTS = {
 "1": "-----BEGIN CERTIFICATE-----\nMIIDBzCCAe+gAwIBAgIJAJV0CZs0xe5nMA0GCSqGSIb3DQEBBQUAMBoxGDAWBgNV\nBAMMD3d3dy5leGFtcGxlLmNvbTAeFw0xNDA0MDYxOTUyMTNaFw0yNDA0MDMxOTUy\nMTNaMBoxGDAWBgNVBAMMD3d3dy5leGFtcGxlLmNvbTCCASIwDQYJKoZIhvcNAQEB\nBQADggEPADCCAQoCggEBANg45Z+qcP0r9wsHwt+K7VqvGUMlzlw10Q0tsEvBkoaT\nPLanSd0d1rRXlK8xp7KcAU6TQ7cp3p6yOvuZEVrk9pAxKZVo+kH3J3HylEaMDzn9\nCwDTfWXulnaTbQinzw8KKfy81aemNG8MFyohZvCX+UlGLG3xjADc9SCgqbhhSasP\nR3gpVM7Af4KqLjbvPVu4Hab1OebdhScc9J4brDindU0gMp9vw3RfDYNy4exsuLHU\noi0RsFZM6TO5hc92C+9UXdZ9yqT5IOnjFCYEXa5KK+Lhi7LqOE1MBerOnWm0xhRA\npeKxl3rbIdMefNqWGZYAc0h1fyGvzvsXOSvBXX0LsusCAwEAAaNQME4wHQYDVR0O\nBBYEFC+DA1Kq6JN2nAjz7keBWL/ZAKIMMB8GA1UdIwQYMBaAFC+DA1Kq6JN2nAjz\n7keBWL/ZAKIMMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAJZ/H9g3\nzEgav0VXEbApWhColU13Bl4Cd5EnapqQ/WCNlRctmsm+Qfpfnpie/U+9uf2eB65G\nFZz6V0EBN61YkD7vsxoLPvBb3s1NGxHY8/6CX0cekqbNFrxYCxMeMnQZnrB+lP5H\nIHN58hQm6pPPehMRi7o19hopGhBaOHnNL/aGfbudpgZcHtw1kzOkGnacDdy870me\no8Mc86FuN0Evh9wZlWVKm7EfFLOHP9UuW0AXEgEVebqB9TqC4LzvUxmiaP4Q+Rk4\nuksqk4J20D/zluqTHFmbDt6XEIxSD0pRsVtZQZZOpq5oJzjaG/HHff5MMCVDIL12\nqfYzKY4W1QWTMhE=\n-----END CERTIFICATE-----\n"
}


class Test(unittest.TestCase):

    def setUp(self):
        self._token_validator = GoogleTokenValidator(AUDIENCE, CLIENT_IDS)

    def test_should_validate_token(self):
        self._token_validator._certificates = self._token_validator._decode_certificates(OLD_GOOGLE_CERTS)
        decoded_token = self._token_validator.validate_token(TOKEN, True, False, False)
        self.assertIsNotNone(decoded_token)

    def test_should_fail_expired_token(self):
        self._token_validator._certificates = self._token_validator._decode_certificates(OLD_GOOGLE_CERTS)
        decoded_token = self._token_validator.validate_token(TOKEN, True, True, False)
        self.assertIsNone(decoded_token)

    def test_should_update_certificates(self):
        self._token_validator._certificates = self._token_validator._decode_certificates(OLD_GOOGLE_CERTS)
        self.assertTrue(self._token_validator._are_certificates_expired())
        self._token_validator.validate_token(TOKEN, True, False)
        self.assertFalse(self._token_validator._are_certificates_expired())

    def test_should_fail_invalid_certificate(self):
        self._token_validator._certificates = self._token_validator._decode_certificates(INVALID_CERTS)
        decoded_token = self._token_validator.validate_token(TOKEN, True, False, False)
        self.assertIsNone(decoded_token)

    def test_should_validate_secret_token(self):
        self._token_validator._secret_token = 'token'
        decoded_token = self._token_validator.validate_token('token', True, True, False)
        self.assertEqual(decoded_token, DECODED_SECRET_TOKEN)
