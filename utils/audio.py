'''
Created on 20 sty 2014

@author: sopel39
'''
import os
import subprocess
import uuid

from utils import file
from utils.temporarydir import TemporaryDirectory


def convert_to_wav(path, remove_source=False, wav_path=None, sample_rate=11025, n_channels=1, tmp_dir=None):
    if not wav_path:
        # use default wav name
        dirname = os.path.dirname(path)
        basename = file.basename_no_suffix(path)
        wav_path = os.path.join(dirname, basename + '.wav')

    with TemporaryDirectory(suffix='_audio', tmp_dir=tmp_dir) as sub_tmp_dir, open(os.devnull, "w") as fnull:
        # convert to wav
        tmp_wav_path = os.path.join(sub_tmp_dir, str(uuid.uuid4()) + '.wav')
        subprocess.check_call(['ffmpeg', '-i', path,
                               '-ar', str(sample_rate),
                               '-ac', str(n_channels),
                               '-acodec', 'pcm_s16le',
                               tmp_wav_path],
                              stdout=fnull, stderr=fnull)

        # FIXME: use sox to correct wav header created by ffmpeg
        subprocess.check_call(['sox', tmp_wav_path, wav_path], stdout=fnull, stderr=fnull)

    # remove source file
    if remove_source:
        os.remove(path)

    return wav_path
