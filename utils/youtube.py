'''
Created on 30 sty 2014

@author: sopel39
'''

import logging
import re
from string import time_to_millis

from lxml import etree
import requests


logger = logging.getLogger(__name__)

def request_html(url, params):
    response = requests.get(url, params=params)
    response.raise_for_status()
    return etree.fromstring(response.text, etree.HTMLParser())

def extract_youtube_id(youtube_url):
    return re.search('https?://www.youtube.com/watch\?v=(.+?)&.*', youtube_url + '&').group(1)

def search_youtube(query):
    # parse search results metadata
    root = request_html('http://www.youtube.com/results', params={'search_query': query})
    videos = root.xpath('//div[contains(@class,"yt-lockup-video")]')
    results = []
    for video in videos:
        try:
            title = video.xpath(".//h3[@class='yt-lockup-title']/a/text()")[0]
            youtube_id = video.xpath(".//h3[@class='yt-lockup-title']/a/@href")[0]
            # ensure correct href
            if not youtube_id.startswith('/watch?v='):
                logger.info('invalid youtube href: %s for title: %s, query: %s, perhaps it is a channel',
                            youtube_id, title, query)
                continue
            youtube_id = youtube_id[len('/watch?v='):]
            # ensure video has length provided
            length = video.xpath(".//span[@class='video-time']")
            if not length:
                logger.info('no length for youtube video (perhaps it is a channel): %s, title: %s, query: %s',
                            youtube_id, title, query)
                continue
            length = time_to_millis(length[0].text)
            results.append({'title': title, 'length': length, 'id': youtube_id})
        except:
            logger.exception('could not parse youtube video (query: %s):\n%s',
                             query, etree.tostring(video, pretty_print=True))
    if not results:
        logger.warn('no youtube results for query: %s', query)
    return results

def get_youtube_title(youtube_id):
    # parse title
    root = request_html('http://www.youtube.com/watch', params={'v': youtube_id})
    return root.xpath('//meta[@name="title"]/@content')[0]
