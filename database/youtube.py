'''
Created on 30 sty 2014

@author: sopel39
'''

import os

from database.recordingsindex import RecordingsIndex
from wang import tasks as fingerprint
from ytdownloader import tasks as ytdownloader


def has_youtube_fingerprint(recording, youtube_id):
    match_id = 'youtube_' + youtube_id
    return match_id in recording[RecordingsIndex.MATCHES]

def add_youtube_fingerprint(recording, youtube_id, youtube_title, download_dir):
    # download youtube video
    video_path = ytdownloader.youtube_download.delay(youtube_id, download_dir).get()

    # add fingerprint
    try:
        match_id = 'youtube_' + youtube_id
        fingerprint.add_fingerprint_to_database.delay(match_id, recording[RecordingsIndex.ID], video_path).wait()
    finally:
        os.remove(video_path)

    # add match entry
    recording[RecordingsIndex.MATCHES].update({
        match_id: {
            RecordingsIndex.NAME: youtube_title,
            RecordingsIndex.TYPE: 'youtube',
            RecordingsIndex.URL: 'http://www.youtube.com/watch?v=' + youtube_id,
            RecordingsIndex.IGNORE: False
        }
    })

