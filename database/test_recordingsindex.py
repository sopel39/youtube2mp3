'''
Created on 25 sty 2014

@author: sopel39
'''
from datetime import datetime
import unittest

from database.recordingsindex import RecordingsIndex


class Test(unittest.TestCase):

    def setUp(self):
        self._index = RecordingsIndex()

    def tearDown(self):
        self._index.close()

    def _get_sample_match(self, match_id='match_id', match_name='match_name', ignored=False):
        return {
            match_id: {
                RecordingsIndex.NAME: match_name,
                RecordingsIndex.TYPE: 'youtube',
                RecordingsIndex.URL: 'http://www.youtube.com/watch?v=epNJX7CNK9g',
                RecordingsIndex.IGNORE: ignored
            }
        }

    def _get_sample_recording(self, recording_id='recording_id', recording_name='recording_name',
                              match_id='match_id', match_name='match_name'):
        return {
            RecordingsIndex.ID: recording_id,
            RecordingsIndex.NAME: recording_name,
            RecordingsIndex.TYPE: 'test_type',
            RecordingsIndex.MATCHES: self._get_sample_match(match_id)
        }

    def _get_empty_recording(self, recording_id='recording_id', recording_name='recording_name'):
        return {
            RecordingsIndex.ID: recording_id,
            RecordingsIndex.NAME: recording_name,
            RecordingsIndex.TYPE: 'test_type'
        }

    def test_should_add_empty(self):
        # add sample recording with no matches
        recording = self._get_sample_recording()
        del recording[RecordingsIndex.MATCHES]
        self._index.add_recording(recording)

        # should not match any recording
        self.assertIsNone(self._index.get_recording_by_match_id('match_id'))

        recording[RecordingsIndex.MATCHES] = {}
        self._index.add_recording(recording)
        self.assertIsNone(self._index.get_recording_by_match_id('match_id'))

    def test_should_add_recording(self):
        # add sample recording
        self._index.add_recording(self._get_sample_recording())

        # match recording
        match_recording = self._index.get_recording_by_match_id('match_id')
        self.assertEqual(match_recording[RecordingsIndex.ID], 'recording_id')
        self.assertEqual(match_recording[RecordingsIndex.NAME], 'recording_name')
        self.assertEqual(match_recording[RecordingsIndex.TYPE], 'test_type')
        self.assertIsInstance(match_recording[RecordingsIndex.CREATEDATE], datetime)
        self.assertEqual(match_recording[RecordingsIndex.CREATEDATE].hour, datetime.now().hour)
        match = match_recording[RecordingsIndex.MATCHES]['match_id']
        self.assertEqual(match[RecordingsIndex.NAME], 'match_name')
        self.assertEqual(match[RecordingsIndex.TYPE], 'youtube')
        self.assertEqual(match[RecordingsIndex.URL], 'http://www.youtube.com/watch?v=epNJX7CNK9g')
        self.assertIsInstance(match[RecordingsIndex.CREATEDATE], datetime)
        self.assertEqual(match[RecordingsIndex.CREATEDATE].hour, datetime.now().hour)
        self.assertFalse(match[RecordingsIndex.IGNORE])

        self.assertIsNone(self._index.get_recording_by_match_id('match2_id'))

    def test_should_merge_recording(self):
        # add sample recording
        self._index.add_recording(self._get_sample_recording())

        # add additional matches
        rec = self._get_sample_recording(match_id='match2_id')
        rec[RecordingsIndex.MATCHES].update(self._get_sample_match('match3_id', ignored=True))
        self._index.add_recording(self._index.merge_recording(rec))

        # ensure that matches were added
        rec = self._index.get_recording('recording_id')
        self.assertEqual(rec[RecordingsIndex.ID], 'recording_id')
        self.assertEqual(rec[RecordingsIndex.NAME], 'recording_name')
        matches = rec[RecordingsIndex.MATCHES]
        self.assertIn('match_id', matches)
        self.assertIn('match2_id', matches)
        self.assertIn('match3_id', matches)
        self.assertEqual(len(matches), 3)

        # assert that ignore flag is preserved
        self.assertTrue(matches['match3_id'][RecordingsIndex.IGNORE])

    def test_should_get_recording(self):
        # add sample recording
        self._index.add_recording(self._get_sample_recording())

        self.assertIsNotNone(self._index.get_recording('recording_id'))
        self.assertIsNone(self._index.get_recording('foo'))

    def test_should_get_recordings(self):
        self._index.add_recording(self._get_sample_recording('rec1_id', 'rec1', 'match1'))
        self._index.add_recording(self._get_sample_recording('rec2_id', 'rec2', 'match2'))

        recs_part1 = self._index.get_recordings(1, 1)[RecordingsIndex.RESULTS]
        recs_all = self._index.get_recordings()[RecordingsIndex.RESULTS]
        recs_part2 = self._index.get_recordings(lower_recording_id=recs_all[0][RecordingsIndex.ID])[RecordingsIndex.RESULTS]

        self.assertEqual(len(recs_part1), 1)
        self.assertGreaterEqual(len(recs_all), 2)
        self.assertLessEqual(recs_all[0][RecordingsIndex.ID], recs_all[1][RecordingsIndex.ID])
        self.assertEqual(recs_part2[0][RecordingsIndex.ID], recs_all[1][RecordingsIndex.ID])

    def test_should_get_empty_recordings(self):
        self._index.add_recording(self._get_empty_recording('rec1_id', 'rec1'))
        self._index.add_recording(self._get_sample_recording('rec2_id', 'rec2', 'match2'))

        empty_recs = self._index.get_empty_recordings()
        self.assertGreaterEqual(len(empty_recs), 1)
        for empty_rec in empty_recs:
            self.assertEqual(len(empty_rec[RecordingsIndex.MATCHES]), 0)

    def test_should_get_matches_count(self):
        self._index.add_recording(self._get_sample_recording())
        self.assertEqual(self._index.get_matches_count('recording_id'), 1)

    def test_should_get_not_ignored_matches_count(self):
        rec = self._get_sample_recording()
        rec[RecordingsIndex.MATCHES].update(self._get_sample_match('match2_id', ignored=True))
        rec[RecordingsIndex.MATCHES].update(self._get_sample_match('match3_id', ignored=False))
        self._index.add_recording(rec)

        self.assertEqual(self._index.get_matches_count('recording_id'), 3)
        rec = self._index.get_recording('recording_id')
        self.assertEqual(rec.get_not_ignored_matches_count(), 2)
        self.assertTrue(rec.is_match_ignored('match2_id'))
        self.assertFalse(rec.is_match_ignored('match3_id'))
        self.assertFalse(rec.is_match_ignored('foo'))

    def test_should_delete_recording(self):
        self._index.add_recording(self._get_sample_recording())
        self.assertIsNotNone(self._index.get_recording('recording_id'))
        self._index.delete_recording('recording_id')
        self.assertIsNone(self._index.get_recording('recording_id'))

