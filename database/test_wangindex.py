'''
Created on 10 lut 2014

@author: sopel39
'''
import random
import unittest

import scipy

from settings.wang import TIME_RANGE, BINS_RANGE, DBINS_BITS, DTIME_BITS
from wangindex import WangIndex


MAX_CODE_LEN = 5

F1 = 'f1'
F2 = 'f2'
F3 = 'f3'
REC1 = 'rec1'
REC2 = 'rec2'
PART1 = 'part1'
PART2 = 'part2'

class Test(unittest.TestCase):

    def setUp(self):
        self._index = WangIndex()

    def tearDown(self):
        self._index.close()

    def gen_random_landmarks(self, peak_bins_from, peak_bins_to, n=10000):
        landmarks = []
        for i in range(n):
            t = i
            t2 = t + random.randint(1, 32)
            peak_bin = 100
            peak_bin2 = peak_bin + random.randint(peak_bins_from, peak_bins_to)
            landmarks.append((t, t2, peak_bin, peak_bin2))
        return landmarks

    def add_sample_hashes(self):
        # add two fingerprints with no common landmarks
        l1 = self.gen_random_landmarks(-64, 0, 10000)
        l2 = self.gen_random_landmarks(1, 64, 10000)
        l1 = scipy.array(l1 + l1)
        l2 = scipy.array(l2 + l2)

        self._index.add_hashes_parts(REC1, F1, [(PART1, l1[l1[:, 0].argsort()]), (PART2, [])])
        self._index.add_hashes(REC2, F2, PART1, l2[l2[:, 0].argsort()])
        self._index.add_hashes(REC2, F2, PART2, [])
        self._index.add_hashes(REC1, F3, PART1, [])

        return (l1, l2)

    def test_find(self):
        # search for hashes for 1nd recording
        l1, _ = self.add_sample_hashes()

        fingerprints = self._index.find_fingerprints(l1[:500])
        self.assertEqual(len(fingerprints), 1)
        key = fingerprints.keys()[0]

        self.assertEqual(key[0], REC1)
        self.assertEqual(key[1], F1)
        self.assertEqual(key[2], PART1)
        self.assertGreaterEqual(len(fingerprints[key]), 1000)

        for time, time_diff in fingerprints[key]:
            self.assertEqual(WangIndex._landmark_to_hash(l1[time]),
                             WangIndex._landmark_to_hash(l1[time + time_diff]))

    def test_count(self):
        # test counting
        l1, l2 = self.add_sample_hashes()
        self.assertEqual(self._index.count_hashes(REC1, F1, PART1), len(l1))
        self.assertEqual(self._index.count_hashes(REC1, F1, PART2), 0)
        self.assertEqual(self._index.count_hashes(REC2, F2, PART1), len(l2))
        self.assertEqual(self._index.count_hashes(REC2, F2, PART2), 0)

        self.assertEqual(len(self._index.get_hashes(REC1, F1, PART1)), len(l1))
        self.assertEqual(len(self._index.get_hashes(REC1, F1, PART2)), 0)
        self.assertEqual(len(self._index.get_hashes(REC2, F2, PART1)), len(l2))
        self.assertEqual(len(self._index.get_hashes(REC2, F2, PART2)), 0)

    def test_remove(self):
        # test removal
        l1, _ = self.add_sample_hashes()

        self.assertTrue(self._index.has_entry(REC1, F1, PART1))
        self.assertTrue(self._index.has_entry(REC1, F1, PART2))
        self.assertTrue(self._index.has_entry(REC1, F3, PART1))

        self.assertTrue(self._index.has_entry(REC2, F2, PART1))
        self.assertTrue(self._index.has_entry(REC2, F2, PART2))

        self._index.remove_fingerprint(REC1, F1)
        self._index.remove_fingerprints(REC2)

        self.assertFalse(self._index.has_entry(REC1, F1, PART1))
        self.assertFalse(self._index.has_entry(REC1, F1, PART2))
        self.assertTrue(self._index.has_entry(REC1, F3, PART1))

        self.assertFalse(self._index.has_entry(REC2, F2, PART1))
        self.assertFalse(self._index.has_entry(REC2, F2, PART2))

        self.assertEqual(len(self._index.find_fingerprints(l1[:500])), 0)

    def test_hashgen(self):
        for t in range(1, TIME_RANGE + 1):
            for b1 in range(256):
                for b2 in range(b1 - BINS_RANGE + 1, b1 + BINS_RANGE):
                    val = WangIndex._landmark_to_hash((0, t, b1, b2))
                    self.assertEqual(val >> (DBINS_BITS + DTIME_BITS), b1)
                    self.assertEqual(((val >> DTIME_BITS) & (2 ** DBINS_BITS - 1)) - 2 ** (DBINS_BITS - 1), b2 - b1)
                    self.assertEqual(val & (2 ** DTIME_BITS - 1), t - 1)
                    self.assertLessEqual(len(hex(long(val))[2:-1]), MAX_CODE_LEN)

