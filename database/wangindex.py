'''
Created on 9 lut 2014

@author: sopel39
'''

import base64
from collections import defaultdict
import string
import zlib

import scipy
import solr

from settings import solr as settings
from settings.wang import DBINS_BITS, DTIME_BITS, N_TOP_RESULTS
from utils.string import tuple_to_str


class WangIndex(object):

    def __init__(self):
        self._conn = solr.SolrConnection(settings.WANG_HASHES_SOLR_URL)
        self._hashq = solr.SearchHandler(self._conn, '/hashq')

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._conn.close()

    @staticmethod
    def _landmark_to_hash((t, t2, peak_bin, peak_bin2)):
        return ((peak_bin << (DBINS_BITS + DTIME_BITS))
                | ((peak_bin2 - peak_bin + 2 ** (DBINS_BITS - 1)) << DTIME_BITS)
                | (t2 - t - 1))

    @staticmethod
    def _get_code(landmarks):
        hashes = [WangIndex._landmark_to_hash(landmark) for landmark in landmarks]
        times = [t for t, _, _, _ in landmarks]
        # generate fp code: <hash> <time> <hash> <time>
        return ' '.join([hex(long(h))[2:-1] + ' ' + str(t) for h, t in zip(hashes, times)])

    @staticmethod
    def _code_to_hashes(code):
        if not code:
            return []
        hashes = [long(h, 16) for h in string.split(code, ' ')[0::2]]
        times = [int(t) for t in string.split(code, ' ')[1::2]]
        return zip(hashes, times)

    @staticmethod
    def _compress(code):
        return base64.b64encode(zlib.compress(code))

    @staticmethod
    def _decompress(code):
        return zlib.decompress(base64.b64decode(code))

    @staticmethod
    def _get_entry_id(recording_id, fingerprint_id, part_id):
        return tuple_to_str((recording_id, fingerprint_id, part_id))

    def add_hashes(self, recording_id, fingerprint_id, part_id, landmarks):
        code = WangIndex._get_code(landmarks)
        entry_id = WangIndex._get_entry_id(recording_id, fingerprint_id, part_id)
        self._conn.add(id=entry_id, recording_id=recording_id, fingerprint_id=fingerprint_id, part_id=part_id,
                       fp=code, code=WangIndex._compress(code), count=len(landmarks))
        self._conn.commit()

    def add_hashes_parts(self, recording_id, fingerprint_id, parts):
        entries = []
        for (part_id, landmarks) in parts:
            code = WangIndex._get_code(landmarks)
            entry_id = WangIndex._get_entry_id(recording_id, fingerprint_id, part_id)
            entries.append({
                'id': entry_id,
                'recording_id': recording_id,
                'fingerprint_id': fingerprint_id,
                'part_id': part_id,
                'fp': code,
                'code': WangIndex._compress(code),
                'count': len(landmarks)
            })
        self._conn.add_many(entries)
        self._conn.commit()

    def get_hashes(self, recording_id, fingerprint_id, part_id):
        entry_id = WangIndex._get_entry_id(recording_id, fingerprint_id, part_id)
        response = self._conn.query('id:"' + entry_id + '"')
        if response.results:
            return WangIndex._code_to_hashes(WangIndex._decompress(response.results[0]['code']))
        else:
            return []

    def find_fingerprints(self, landmarks):
        """ Returns a list of hashes ((recording_id, fingerprint_id, part_id), time, time_diff)
        that match given landmarks.
        """
        # build hashes map
        hashes = defaultdict(list)
        for landmark in landmarks:
            hashes[WangIndex._landmark_to_hash(landmark)].append(landmark)
        # search for hashes that match given landmarks
        code = WangIndex._get_code(landmarks)
        response = self._hashq(code, rows=N_TOP_RESULTS)
        # compute time deltas for matches
        results = []
        for match in response.results:
            recording_id = match['recording_id']
            fingerprint_id = match['fingerprint_id']
            part_id = match['part_id']
            entry_descr = (recording_id, fingerprint_id, part_id)
            times = []
            for h, t in WangIndex._code_to_hashes(WangIndex._decompress(match['code'])):
                for landmark in hashes[h]:
                    times.append((t, landmark[0] - t))
            results.append((entry_descr, times))
        return {entry_descr: scipy.array(times)
                for entry_descr, times in results}

    def remove_fingerprint(self, recording_id, fingerprint_id):
        self._conn.delete_query('recording_id:"' + recording_id +
                                '" AND fingerprint_id:"' + fingerprint_id + '"')
        self._conn.commit()

    def remove_fingerprints(self, recording_id):
        self._conn.delete_query('recording_id:"' + recording_id + '"')
        self._conn.commit()

    def count_hashes(self, recording_id, fingerprint_id, part_id):
        # count hashes for a given entry
        entry_id = WangIndex._get_entry_id(recording_id, fingerprint_id, part_id)
        response = self._conn.query('id:"' + entry_id + '"', fl='count')
        if response.results:
            return response.results[0]['count']
        else:
            return 0

    def has_entry(self, recording_id, fingerprint_id, part_id):
        entry_id = WangIndex._get_entry_id(recording_id, fingerprint_id, part_id)
        response = self._conn.query('id:"' + entry_id + '"', fl='count')
        return True if response.results else False

    def close(self):
        self._conn.close()

