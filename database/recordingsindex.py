'''
Created on 20 sty 2014

@author: sopel39
'''

from datetime import datetime

import pytz
import solr
import tzlocal

from settings import solr as settings


class Recording(dict):

    @staticmethod
    def of(recording_id, recording_name, recording_type, recording_createdate=None, matches=None):
        recording = Recording()
        recording[RecordingsIndex.ID] = recording_id
        recording[RecordingsIndex.NAME] = recording_name
        recording[RecordingsIndex.TYPE] = recording_type
        if recording_createdate:
            recording[RecordingsIndex.CREATEDATE] = recording_createdate
        recording[RecordingsIndex.MATCHES] = matches if matches else {}
        return recording

    def get_not_ignored_matches_count(self):
        return reduce(lambda acc, match: acc + int(not match[RecordingsIndex.IGNORE]),
                      self[RecordingsIndex.MATCHES].values(), 0)

    def is_match_ignored(self, match_id):
        matches = self[RecordingsIndex.MATCHES]
        return match_id in matches and matches[match_id][RecordingsIndex.IGNORE]

class RecordingsIndex(object):
    # field names
    ID = 'id'
    NAME = 'name'
    CREATEDATE = 'createdate'
    MATCHES = 'matches'
    TYPE = 'type'
    URL = 'url'
    IGNORE = 'ignore'
    RESULTS = 'results'
    NUM_FOUND = 'num_found'

    RECORDING_ID_COL = 'id'
    RECORDING_NAME_COL = 'name'
    RECORDING_NAME_SORT_COL = 'name_sort'
    RECORDING_TYPE_COL = 'type'
    RECORDING_CREATEDATE_COL = 'createdate'
    MATCH_IDS_COL = 'match_ids'
    MATCH_NAMES_COL = 'match_names'
    MATCH_TYPES_COL = 'match_types'
    MATCH_URLS_COL = 'match_urls'
    MATCH_CREATEDATES_COL = 'match_createdates'
    MATCH_IGNORES_COL = 'match_ignores'
    MATCHES_COUNT_COL = 'count'


    def __init__(self):
        self._conn = solr.SolrConnection(settings.RECORDINGS_INDEX_SOLR_URL)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._conn.close()

    def add_recording(self, recording):
        """ Adds a recording to a database.
    
        Args:
            recording -- a recording dictionary (e.g 
                {'name': 'rec_name', 'type': 'brainz', createdate': rec_date, 'matches':
                     {'match_id1': {'type': 'youtube', 'name': 'match_name', 'url': 'www.*',
                                    'createdate': match_date, 'ignore': true/false}}})

        """

        # extract document fields
        recording_id = recording[self.ID]
        recording_name = recording[self.NAME]
        recording_type = recording[self.TYPE]
        now = pytz.utc.localize(datetime.utcnow())
        createdate = recording[self.CREATEDATE] if self.CREATEDATE in recording else now

        # flatten match entries
        match_ids = []
        match_names = []
        match_types = []
        match_urls = []
        match_createdates = []
        match_ignores = []
        if self.MATCHES in recording:
            for match_id, match in recording[self.MATCHES].items():
                match_ids.append(match_id)
                match_names.append(match[self.NAME])
                match_types.append(match[self.TYPE])
                match_urls.append(match[self.URL])
                match_createdates.append(
                    match[self.CREATEDATE] if self.CREATEDATE in match else now)
                match_ignores.append(match[self.IGNORE])

        # save document
        doc = {
            self.RECORDING_ID_COL: recording_id,
            self.RECORDING_NAME_COL: recording_name,
            self.RECORDING_NAME_SORT_COL: recording_name,
            self.RECORDING_TYPE_COL: recording_type,
            self.RECORDING_CREATEDATE_COL: createdate,
            self.MATCH_IDS_COL: match_ids,
            self.MATCH_NAMES_COL: match_names,
            self.MATCH_TYPES_COL: match_types,
            self.MATCH_URLS_COL: match_urls,
            self.MATCH_CREATEDATES_COL: match_createdates,
            self.MATCH_IGNORES_COL: match_ignores,
            self.MATCHES_COUNT_COL: len(match_ids)
        }
        self._conn.add(**doc)
        self._conn.commit()

    def _to_local_datetime(self, dt):
        return dt.astimezone(tzlocal.get_localzone())

    def _extract_recording(self, solr_result):
        # build recording dictionary
        recording = Recording()
        recording[self.ID] = solr_result[self.RECORDING_ID_COL]
        recording[self.NAME] = solr_result[self.RECORDING_NAME_COL]
        recording[self.TYPE] = solr_result[self.RECORDING_TYPE_COL]
        recording[self.CREATEDATE] = self._to_local_datetime(solr_result[self.RECORDING_CREATEDATE_COL])

        # add matches
        matches = {}
        n_matches = len(solr_result[self.MATCH_IDS_COL]) if self.MATCH_IDS_COL in solr_result else 0
        for i in range(n_matches):
            matches[solr_result[self.MATCH_IDS_COL][i]] = {
                self.NAME: solr_result[self.MATCH_NAMES_COL][i],
                self.TYPE: solr_result[self.MATCH_TYPES_COL][i],
                self.URL: solr_result[self.MATCH_URLS_COL][i],
                self.CREATEDATE: self._to_local_datetime(solr_result[self.MATCH_CREATEDATES_COL][i]),
                self.IGNORE: solr_result[self.MATCH_IGNORES_COL][i]
            }
        recording[self.MATCHES] = matches
        return recording

    def get_recording(self, recording_id):
        # query for recording
        response = self._conn.query(self.RECORDING_ID_COL + ':"' + recording_id + '"')
        if response.results:
            return self._extract_recording(response.results[0])
        return None

    def get_recording_by_match_id(self, match_id):
        # query for recordings
        response = self._conn.query(self.MATCH_IDS_COL + ':"' + match_id + '"')
        if response.results:
            return self._extract_recording(response.results[0])
        return None

    def merge_recording(self, recording):
        index_recording = self.get_recording(recording[self.ID])
        if not index_recording:
            return recording
        else:
            # merge all matches
            index_recording[self.MATCHES].update(recording[self.MATCHES])
            return index_recording

    def get_recordings(self, start=0, rows=1000000000, lower_recording_id=None, and_query=None, sort='id asc'):
        lower_recording_id = '"' + lower_recording_id + '"' if lower_recording_id else "*"
        query = self.RECORDING_ID_COL + ':{' + lower_recording_id + ' TO *]'
        # concatenate and query parameter
        if and_query:
            query += ' AND ' + and_query
        response = self._conn.query(query, start=start, rows=rows, sort=sort)
        return {
            self.RESULTS: [self._extract_recording(result) for result in response.results],
            self.NUM_FOUND: response.numFound
        }

    def get_total_recordings(self):
        return self.get_recordings(rows=0)[self.NUM_FOUND]

    def get_empty_recordings(self, start=0, rows=1000000000):
        response = self._conn.query(self.MATCHES_COUNT_COL + ':0', start=start, rows=rows)
        return [self._extract_recording(result) for result in response.results]

    def get_matches_count(self, recording_id):
        response = self._conn.query(self.RECORDING_ID_COL + ':"' + recording_id + '"', fl=self.MATCHES_COUNT_COL)
        if response.results:
            return response.results[0][self.MATCHES_COUNT_COL]
        else:
            return 0

    def delete_recording(self, recording_id):
        self._conn.delete(id=recording_id)
        self._conn.commit()

    def close(self):
        self._conn.close()
