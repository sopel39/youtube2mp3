#!/bin/bash
AUDFPRINT_DBASE=dbase/matcher.mat celery worker -l INFO --pool=eventlet -n audfprint_matcher$MATCHER_NUM.%h \
 -Q audfprint_matcher,audfprint_matcher_broadcast --app=audfprint.app
