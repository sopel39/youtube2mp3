'''
Created on 17 sty 2014

@author: sopel39
'''

import sys

from celery import Celery


app = Celery('audfprint', include=['audfprint.tasks'])
app.config_from_object('settings.celery')
app.conf.update(
    CELERYD_CONCURRENCY=1  # single audfprint process per one matcher/watcher worker
)

if __name__ == '__main__':
    app.start(sys.argv)
