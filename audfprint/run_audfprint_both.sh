#!/bin/bash
celery worker -l INFO --pool=eventlet -n audfprint_both.%h \
 -Q audfprint_matcher,audfprint_matcher_broadcast,audfprint_watcher --app=audfprint.app
