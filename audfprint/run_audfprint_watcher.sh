#!/bin/bash
AUDFPRINT_DBASE=dbase/watcher.mat celery worker -l INFO --pool=eventlet -n audfprint_watcher.%h \
 -Q audfprint_watcher --app=audfprint.app
