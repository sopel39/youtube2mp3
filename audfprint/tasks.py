'''
Created on 12 sty 2014

@author: sopel39
'''

import logging
import shutil

from celery.signals import worker_init, worker_shutdown

from app import app
from service import Service
from settings.audfprint import DBASE
from settings.celery import AUDFPRINT_WATCHER_QUEUE, AUDFPRINT_MATCHER_QUEUE, \
    AUDFPRINT_MATCHER_BROADCAST


logger = logging.getLogger(__name__)

@worker_init.connect()
def worker_init(**kwargs):
    global service
    service = Service()
    service.start()

@worker_shutdown.connect()
def worker_shutdown(**kwargs):
    service.stop()

@app.task(queue=AUDFPRINT_WATCHER_QUEUE)
def add_fingerprint_to_database(fingerprint_id, short_descr, path):
    service.add_fingerprint_to_database(fingerprint_id, short_descr, path)

@app.task(queue=AUDFPRINT_MATCHER_QUEUE)
def match_fingerprint(path):
    return service.match_fingerprint(path)

@app.task(queue=AUDFPRINT_WATCHER_QUEUE)
def save_database(dbase_path):
    logger.info('saving database to: %s', dbase_path)
    service.stop()
    shutil.copyfile(DBASE, dbase_path)
    service.start()

@app.task(queue=AUDFPRINT_MATCHER_BROADCAST, ignore_result=True)
def load_database(dbase_path):
    logger.info('loading database: %s', dbase_path)
    service.stop()
    service.start(dbase_path)
