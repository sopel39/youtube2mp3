'''
Created on 18 sty 2014

@author: sopel39
'''
import os
import subprocess
import time
import unittest

import audfprint.tasks as audfprint
from settings.celery import AUDFPRINT_WATCHER_QUEUE, AUDFPRINT_MATCHER_QUEUE, \
    AUDFPRINT_MATCHER_BROADCAST
from utils import timeout


CMD_DELAY = 120

class TestIntegration(unittest.TestCase):

    def _startCelery(self, dbase_path, queue_names):
        celery_env = os.environ.copy()
        celery_env['AUDFPRINT_DBASE'] = dbase_path
        celery_proc = subprocess.Popen(['celery',
                                    'worker',
                                    '-Q', queue_names,
                                    '--app=audfprint.app',
                                    '-l', 'INFO',
                                    '--pool=eventlet'],
                                   env=celery_env)
        return celery_proc

    @timeout.timeout(CMD_DELAY)
    def _wait_timeout(self, proc):
        proc.wait()

    def test_integration(self):
        # start watcher
        self._watcher = self._startCelery('dbase/watcher_test.mat', AUDFPRINT_WATCHER_QUEUE)
        # start matcher
        matcher_queue_names = AUDFPRINT_MATCHER_QUEUE + ',' + AUDFPRINT_MATCHER_BROADCAST
        self._matcher1 = self._startCelery('dbase/matcher_start_test.mat', matcher_queue_names)
        # add audio sample
        result = audfprint.add_fingerprint_to_database.delay('bugs', 'bugs_desc', 'data/audios/bugs-full.mp3')
        result.get(CMD_DELAY)
        # save database with music signature
        result = audfprint.save_database.delay('dbase/matcher_test.mat')
        result.get(CMD_DELAY)
        # reload matchers database
        audfprint.load_database.delay('dbase/matcher_test.mat')
        # match audio sample
        time.sleep(5)
        result = audfprint.match_fingerprint.delay('data/audios/bugs.mp3')
        matches = result.get(2 * CMD_DELAY)
        self.assertEqual(matches[0][0], 'bugs')
        # stop processes
        self._watcher.send_signal(subprocess.signal.SIGINT)
        self._matcher1.send_signal(subprocess.signal.SIGINT)
        self._wait_timeout(self._watcher)
        self._wait_timeout(self._matcher1)
        self.assertEqual(self._watcher.returncode, 0)
        self.assertEqual(self._matcher1.returncode, 0)

    def tearDown(self):
        try:
            self._watcher.send_signal(subprocess.signal.SIGINT)
            self._matcher1.send_signal(subprocess.signal.SIGINT)
            self._wait_timeout(self._watcher)
            self._wait_timeout(self._matcher1)
        except Exception:
            pass

        try:
            os.remove('dbase/watcher_test.mat')
            os.remove('dbase/matcher_start_test.mat')
            os.remove('dbase/matcher_test.mat')
        except Exception:
            pass

