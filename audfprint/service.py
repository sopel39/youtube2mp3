'''
Created on 8 sty 2014

@author: sopel39
'''
import logging
import os
import shutil
import subprocess
import tempfile
import uuid

import settings.audfprint as settings
from utils import audio, file, timeout


logger = logging.getLogger(__name__)

class Service(object):
    def __init__(self):
        self._process = None

    @timeout.timeout(settings.TIMEOUT)
    def start(self, dbase_path=settings.DBASE):
        if self._process:
            return
        logger.info('starting audfprint service')

        # init audfprint database
        if not os.path.isfile(dbase_path):
            dbase_dir = os.path.dirname(dbase_path)
            if not os.path.exists(dbase_dir):
                os.makedirs(dbase_dir)
            subprocess.check_call([settings.AUDFPRINT_BIN,
                                   '-dbase', dbase_path,
                                   '-cleardbase', '1',
                                   '-density', str(settings.DENSITY)])
        self._dbase_path = dbase_path

        # creates match and watch directories
        self._watch_dir = tempfile.mkdtemp('_watch')
        self._match_dir = tempfile.mkdtemp('_match')
        self._match_out_dir = tempfile.mkdtemp('_match_out')

        # start audfprint
        self._process = subprocess.Popen([settings.AUDFPRINT_BIN,
                                          '-dbase', dbase_path,
                                          '-density', str(settings.DENSITY),
                                          '-matchmincount', str(settings.MATCHMINCOUNT),
                                          '-matchdir', self._match_dir,
                                          '-adddir', self._watch_dir,
                                          '-addcheckpoint', str(settings.ADDCHECKPOINT),
                                          '-outdir', self._match_out_dir],
                                         stdout=subprocess.PIPE)
        self._nadded = 0
        self._nmatched = 0

        # wait until audfprint is ready
        file.match_line_start(self._process.stdout, 'Hash table read')

    @timeout.timeout(settings.TIMEOUT)
    def stop(self):
        if not self._process:
            return
        logger.info('stopping audfprint service')
        self._process.send_signal(subprocess.signal.SIGINT)
        self._process.wait()
        self._process = None
        logger.info('audfprint service stopped')

        # remove match and watch directories
        shutil.rmtree(self._watch_dir)
        shutil.rmtree(self._match_dir)
        shutil.rmtree(self._match_out_dir)

    @timeout.timeout(settings.TIMEOUT)
    def add_fingerprint_to_database(self, fingerprint_id, short_descr, path):
        if not self._process:
            raise Exception("audfprint not running")

        logger.info('adding file: %s, fingerprint_id: %s, short_descr: %s', path, str(fingerprint_id), short_descr)

        # make a copy of a file
        ext = file.extension(path)
        copy_path = os.path.join(tempfile.gettempdir(), str(uuid.uuid4()) + ext)
        shutil.copy(path, copy_path)

        try:
            # convert to wav
            if ext != '.wav':
                copy_path = audio.convert_to_wav(copy_path, True)

            # let audfprint add file to a database
            filename = str(fingerprint_id) + "&" + short_descr + '.wav'
            copy_path = file.movefile(copy_path, os.path.join(self._watch_dir, filename))
            file.match_line_start(self._process.stdout, 'added 1 tracks')
            self._nadded += 1

            # check if a database was saved occasionally
            if self._nadded > settings.ADDCHECKPOINT:
                logger.info('saving hash table')
                file.match_line_start(self._process.stdout, 'Hash table saved')
                self._nadded = 0
        finally:
            os.remove(copy_path)

    @timeout.timeout(settings.TIMEOUT)
    def match_fingerprint(self, path):
        if not self._process:
            raise Exception("audfprint not running")

        logger.info('matching file: %s', path)
        self._nmatched += 1

        # make a copy of a file
        ext = file.extension(path)
        copy_path = os.path.join(tempfile.gettempdir(), str(uuid.uuid4()) + ext)
        shutil.copy(path, copy_path)

        try:
            # convert to wav
            if ext != '.wav':
                copy_path = audio.convert_to_wav(copy_path, True)

            # move a copy of a file to a match directory
            filename = str(self._nmatched) + '.wav'
            copy_path = file.movefile(copy_path, os.path.join(self._match_dir, filename))

            # wait for matches
            matches_filename = file.basename_no_suffix(filename) + '.txt'
            matches_path = os.path.join(self._match_out_dir, matches_filename)
            file.wait_file(matches_path)

            try:
                # extract matches
                matches = []
                for line in open(matches_path, 'r'):
                    tokens = line.split()
                    match_filename = file.basename_no_suffix(tokens[2])
                    match_tokens = match_filename.split('&')
                    matches.append((match_tokens[0], match_tokens[1]))
            finally:
                os.remove(matches_path)
        finally:
            os.remove(copy_path)

        return matches
