package com.youtube2mp3.monitor;

import static com.youtube2mp3.wang.LandmarkConsts.FFT_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.FFT_TIME;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.ShortBuffer;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.youtube2mp3.monitor.ContinuousLandmarks.LandmarksConsumer;
import com.youtube2mp3.wang.LandmarksCalculator;
import com.youtube2mp3.wang.LandmarksCalculator.Landmark;
import com.youtube2mp3.wang.LandmarksCalculatorFactory;

@RunWith(MockitoJUnitRunner.class)
public class ContinuousLandmarksTest {

    private static final int N_OFFSETS = 2;

    @Mock
    private LandmarksCalculatorFactory calculatorFactory;
    @Mock
    private LandmarksCalculator calculator;
    @Mock
    private LandmarksConsumer consumer;

    private ContinuousLandmarks continuousLandmarks;

    private short[] samples = getDummySamples();

    @Before
    public void setup() {
        when(calculatorFactory.getNewCalculator(anyInt(), anyInt())).thenReturn(calculator);
        when(calculator.pushBuffer(any(ShortBuffer.class))).thenReturn(false);

        continuousLandmarks = new ContinuousLandmarks(consumer, FFT_TIME / 1000d, FFT_TIME / 1000d, N_OFFSETS,
                calculatorFactory);
    }

    @Test
    public void shouldNotProcessEmptySamples() {
        // empty buffer shuld not cause new calculators to be created
        continuousLandmarks.pushBuffer(ShortBuffer.allocate(0));
        verify(calculatorFactory, never()).getNewCalculator(anyInt(), anyInt());
    }

    @Test
    public void shouldProcessSamplesWithOffsets() throws Exception {
        // 2 calculators should be created for 2 offsets (0 and FFT_SIZE / 2)
        ShortBuffer buffer = ShortBuffer.wrap(samples, 0, FFT_SIZE / 2);
        continuousLandmarks.pushBuffer(buffer);
        verify(calculatorFactory, times(1)).getNewCalculator(0, 1);
        verify(calculatorFactory, times(1)).getNewCalculator(FFT_SIZE / 2, 1);
        assertThat(buffer.hasRemaining(), is(false));

        // samples buffer should be pushed to both calculators
        verify(calculator, times(2)).pushBuffer(ShortBuffer.wrap(samples, 0, FFT_SIZE / 2));

        // landmarks from two calculators should be gathered
        continuousLandmarks.processSamples();
        verify(calculator, times(2)).getLandmarks();
        verify(consumer, times(1)).consume(0d, new ArrayList<Landmark>());
    }

    @Test
    public void shouldPushIntersectingSamples() throws Exception {
        continuousLandmarks = new ContinuousLandmarks(consumer, FFT_TIME / 1000d, FFT_TIME / 2 / 1000d, 1,
                calculatorFactory);

        // 2 calculators should be created for 0 offset
        ShortBuffer buffer = ShortBuffer.wrap(samples, 0, FFT_SIZE);
        continuousLandmarks.pushBuffer(buffer);
        verify(calculatorFactory, times(2)).getNewCalculator(0, 1);
        assertThat(buffer.hasRemaining(), is(false));

        // one full samples buffer should be pushed and one with upper samples
        // half
        verify(calculator, times(1)).pushBuffer(ShortBuffer.wrap(samples, 0, FFT_SIZE));
        verify(calculator, times(1)).pushBuffer(ShortBuffer.wrap(samples, FFT_SIZE / 2, FFT_SIZE / 2));

        // landmarks from two calculators should be gathered
        continuousLandmarks.processSamples();
        continuousLandmarks.processSamples();
        verify(calculator, times(2)).getLandmarks();

        // consumer should be called for first and second calculator
        verify(consumer, times(1)).consume(0d, new ArrayList<Landmark>());
        verify(consumer, times(1)).consume(FFT_TIME / 2 / 1000d, new ArrayList<Landmark>());
    }

    @Test
    public void shouldProcessFilledCalculator() throws Exception {
        when(calculator.pushBuffer(any(ShortBuffer.class))).thenReturn(true, false);

        continuousLandmarks = new ContinuousLandmarks(consumer, FFT_TIME / 1000d, FFT_TIME / 1000d, 1,
                calculatorFactory);

        // 1 calculator should be created for 0 offset
        continuousLandmarks.pushBuffer(ShortBuffer.wrap(samples, 0, FFT_SIZE / 2));
        continuousLandmarks.pushBuffer(ShortBuffer.wrap(samples, FFT_SIZE / 2, FFT_SIZE / 2));
        verify(calculatorFactory, times(1)).getNewCalculator(0, 1);

        // calculator should be called with both buffers
        verify(calculator, times(1)).pushBuffer(ShortBuffer.wrap(samples, 0, FFT_SIZE / 2));
        verify(calculator, times(1)).pushBuffer(ShortBuffer.wrap(samples, FFT_SIZE / 2, FFT_SIZE / 2));

        // landmarks from calculator should be gathered
        continuousLandmarks.processSamples();
        verify(calculator, times(1)).getLandmarks();
        verify(consumer, times(1)).consume(0d, new ArrayList<Landmark>());
    }

    private short[] getDummySamples() {
        short[] array = new short[FFT_SIZE];
        for (int i = 0; i < FFT_SIZE; ++i) {
            array[i] = (short) i;
        }
        return array;
    }
}
