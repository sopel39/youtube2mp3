package com.youtube2mp3.monitor;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.entity.ContentType.DEFAULT_BINARY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerClient {

    private static final Logger LOG = LoggerFactory.getLogger(ServerClient.class);

    private static final int DEFAULT_CONNECTION_TIMEOUT = 10 * 1000; // 10s

    public static class RecognizedSong {
        public final List<String> nameParts;
        public final String url;

        public RecognizedSong(List<String> nameParts, String url) {
            this.nameParts = unmodifiableList(nameParts);
            this.url = url;
        }

        @Override
        public String toString() {
            return "RecognizedSong [nameParts=" + nameParts + ", url=" + url + "]";
        }
    }

    private final String queryUrl;
    private final String token;
    private final HttpClient client;

    public ServerClient(String queryUrl, String token, int connectionTimeout) {
        this.queryUrl = queryUrl;
        this.token = token;
        client = UntrustedSSLSocketFactory.getUntrustedHttpClient(new PoolingClientConnectionManager());
        HttpConnectionParams.setConnectionTimeout(client.getParams(), connectionTimeout);
    }

    public ServerClient(String queryUrl, String token) {
        this(queryUrl, token, DEFAULT_CONNECTION_TIMEOUT);
    }

    public List<RecognizedSong> makeQuery(byte[] landmarksBytes) throws IOException {
        // make recognize query
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        LOG.debug("landmarks size: {}", landmarksBytes.length);
        builder.addBinaryBody("landmarks", landmarksBytes, DEFAULT_BINARY, "landmarks");
        builder.addTextBody("token", token);
        HttpPost post = new HttpPost(queryUrl);
        post.setEntity(builder.build());
        HttpResponse response = client.execute(post);
        HttpEntity entity = response.getEntity();
        String responseString = entity != null ? EntityUtils.toString(response.getEntity()) : null;
        // check response status code
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != SC_OK) {
            throw new IOException("invalid response status");
        }
        // parse response JSON
        JSONObject result = new JSONObject(responseString);
        // check response status
        if (!result.getString("status").equals("ok")) {
            throw new IOException("invalid response status");
        }
        JSONArray songs = result.getJSONArray("results");
        if (songs.length() >= 1) {
            JSONObject song = (JSONObject) songs.get(0);
            List<String> songNameParts = extractSongNameParts(song.getJSONArray("name"));
            String songURL = song.getString("url");
            return asList(new RecognizedSong(songNameParts, songURL));
        } else {
            // no songs recognized
            return new ArrayList<RecognizedSong>();
        }
    }

    private List<String> extractSongNameParts(JSONArray songNamePartsArray) {
        List<String> songNameParts = new ArrayList<String>(songNamePartsArray.length());
        for (int i = 0; i < songNamePartsArray.length(); ++i) {
            songNameParts.add(songNamePartsArray.getString(i));
        }
        return songNameParts;
    }
}
