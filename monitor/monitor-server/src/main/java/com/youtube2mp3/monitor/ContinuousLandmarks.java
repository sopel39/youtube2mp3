package com.youtube2mp3.monitor;

import static com.youtube2mp3.wang.LandmarkConsts.FFT_HOP_DIFF;
import static com.youtube2mp3.wang.LandmarkConsts.FFT_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.HOP_SIZE;
import static com.youtube2mp3.wang.LandmarkConsts.N_OFFSETS;
import static com.youtube2mp3.wang.LandmarkConsts.SAMPLE_RATE;
import static java.lang.Math.max;

import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.youtube2mp3.wang.LandmarksCalculator;
import com.youtube2mp3.wang.LandmarksCalculator.Landmark;
import com.youtube2mp3.wang.LandmarksCalculatorFactory;

public class ContinuousLandmarks {

    private static final int DEFAULT_FILLED_CALCULATORS_QUEUE_SIZE = 10;

    public interface LandmarksConsumer {
        void consume(double time, Collection<Landmark> landmarks);
    }

    private class CalculatorGroup {
        final long startingOffset;
        final List<LandmarksCalculator> calculators;
        final boolean last;

        public CalculatorGroup(long startingOffset, List<LandmarksCalculator> calculators, boolean last) {
            this.startingOffset = startingOffset;
            this.calculators = calculators;
            this.last = last;
        }
    }

    private final LandmarksConsumer consumer;
    private final List<CalculatorGroup> calculatorGroups;
    private final BlockingQueue<CalculatorGroup> filledCalculatorGroups;
    private final int landmarkRows;
    private final int intervalSamples;
    private final int nOffsets;
    private final LandmarksCalculatorFactory calculatorFactory;

    private long nextCalculatorGroupStartingOffset;
    private long currentSample;

    public ContinuousLandmarks(LandmarksConsumer consumer, double duration, double interval, int nOffsets,
            LandmarksCalculatorFactory calculatorFactory) {
        this.consumer = consumer;
        this.calculatorGroups = new ArrayList<CalculatorGroup>();
        this.filledCalculatorGroups = new ArrayBlockingQueue<CalculatorGroup>(DEFAULT_FILLED_CALCULATORS_QUEUE_SIZE);
        this.landmarkRows = (int) ((duration * SAMPLE_RATE - FFT_HOP_DIFF) / HOP_SIZE);
        this.intervalSamples = (int) (interval * SAMPLE_RATE);
        this.nOffsets = nOffsets;
        this.calculatorFactory = calculatorFactory;
    }

    public ContinuousLandmarks(LandmarksConsumer consumer, double duration, double interval,
            LandmarksCalculatorFactory calculatorFactory) {
        this(consumer, duration, interval, N_OFFSETS, calculatorFactory);
    }

    public void pushBuffer(ShortBuffer buffer) {
        long toSample = currentSample + buffer.remaining();
        generateCalculatorGroups(toSample);
        fillCalculatorGroups(buffer, toSample);
        currentSample = toSample;
        buffer.position(buffer.limit());
    }

    public boolean processSamples() throws InterruptedException {
        CalculatorGroup calculatorGroup = filledCalculatorGroups.take();
        if (calculatorGroup.last) {
            return false;
        }

        Collection<Landmark> landmarks = new ArrayList<Landmark>();
        for (LandmarksCalculator calculator : calculatorGroup.calculators) {
            landmarks.addAll(calculator.getLandmarks());
        }
        consumer.consume((double) calculatorGroup.startingOffset / SAMPLE_RATE, landmarks);
        return true;
    }

    public void finishSamplesProcessing() throws InterruptedException {
        filledCalculatorGroups.put(new CalculatorGroup(0L, null, true));
    }

    private void fillCalculatorGroups(ShortBuffer buffer, long toSample) {
        for (Iterator<CalculatorGroup> iter = calculatorGroups.iterator(); iter.hasNext();) {
            CalculatorGroup calculatorGroup = iter.next();
            if (fillCalculatorGroup(calculatorGroup, buffer, toSample)) {
                iter.remove();
                filledCalculatorGroups.add(calculatorGroup);
            }
        }
    }

    private boolean fillCalculatorGroup(CalculatorGroup calculatorGroup, ShortBuffer buffer, long toSample) {
        if (toSample <= calculatorGroup.startingOffset) {
            return false;
        }

        boolean filled = true;
        for (LandmarksCalculator calculator : calculatorGroup.calculators) {
            ShortBuffer intersectingSamples = buffer.slice();
            intersectingSamples.position((int) max(0, calculatorGroup.startingOffset - currentSample));
            filled = !calculator.pushBuffer(intersectingSamples) && filled;
        }
        return filled;
    }

    private void generateCalculatorGroups(long toSample) {
        while (nextCalculatorGroupStartingOffset < toSample) {
            List<LandmarksCalculator> calculators = new ArrayList<LandmarksCalculator>(nOffsets);
            for (int offset = 0; offset < FFT_SIZE; offset += FFT_SIZE / nOffsets) {
                calculators.add(calculatorFactory.getNewCalculator(offset, landmarkRows));
            }
            calculatorGroups.add(new CalculatorGroup(nextCalculatorGroupStartingOffset, calculators, false));
            nextCalculatorGroupStartingOffset += intervalSamples;
        }
    }
}
