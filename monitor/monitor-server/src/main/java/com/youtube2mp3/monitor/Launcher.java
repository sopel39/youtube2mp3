package com.youtube2mp3.monitor;

import static com.youtube2mp3.wang.LandmarkConsts.SAMPLE_RATE;
import static com.youtube2mp3.wang.LandmarksUtils.serializeLandmarks;
import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.DiscardPolicy;

import org.apache.log4j.BasicConfigurator;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.youtube2mp3.monitor.ContinuousLandmarks.LandmarksConsumer;
import com.youtube2mp3.monitor.ServerClient.RecognizedSong;
import com.youtube2mp3.wang.LandmarksCalculator.Landmark;
import com.youtube2mp3.wang.WangLandmarksCalculatorFactory;

public class Launcher implements LandmarksConsumer {

    static {
        Loader.load(opencv_core.class);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

    private static final double DEFAULT_DURATION = 3d;
    private static final double DEFAULT_INTERVAL = 1.5d;
    private static final int DEFAULT_OFFSETS = 4;
    // data for 1/10th of a second
    private static final int DEFAULT_SAMPLES_BUFFER_SIZE = SAMPLE_RATE / 10 * 2;
    private static final int DEFAULT_QUERIES = 2;
    private static final String QUERY_API = "/api/query_landmarks/";

    private final ReadableByteChannel inputChannel;
    private final ContinuousLandmarks continuousLandmarks;
    private final ServerClient serverClient;
    private final ExecutorService executorService;
    private final ByteBuffer samplesBuffer;

    private Thread samplesThread;

    private Launcher(double duration, double interval, int nOffsets, String token, String hostname) {
        inputChannel = Channels.newChannel(System.in);
        String queryUrl = "https://" + hostname + QUERY_API;
        continuousLandmarks = new ContinuousLandmarks(this, duration, interval, nOffsets,
                new WangLandmarksCalculatorFactory());
        serverClient = new ServerClient(queryUrl, token);
        executorService = new ThreadPoolExecutor(DEFAULT_QUERIES, DEFAULT_QUERIES, 0L, SECONDS,
                new ArrayBlockingQueue<Runnable>(1), new DiscardPolicy());
        samplesBuffer = ByteBuffer.allocateDirect(DEFAULT_SAMPLES_BUFFER_SIZE);
        samplesBuffer.order(LITTLE_ENDIAN);
    }

    public void run() throws IOException, InterruptedException {
        runSamplesProcessing();

        while (true) {
            int readBytes = inputChannel.read(samplesBuffer);
            if (readBytes == -1) {
                break;
            }

            if (!samplesBuffer.hasRemaining()) {
                samplesBuffer.flip();
                continuousLandmarks.pushBuffer(samplesBuffer.asShortBuffer());
            }
        }

        continuousLandmarks.finishSamplesProcessing();
        samplesThread.join();

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, SECONDS);

        // ignore other ExecutorServices
        System.exit(0);
    }

    private void runSamplesProcessing() {
        samplesThread = new Thread(new Runnable() {
            public void run() {
                try {
                    while (continuousLandmarks.processSamples()) {
                    }
                } catch (Exception e) {
                    LOGGER.info("samples processing exception", e);
                }
            }
        });
        samplesThread.start();
    }

    @Override
    public void consume(final double time, final Collection<Landmark> landmarks) {
        executorService.submit(new Runnable() {
            public void run() {
                try {
                    queryLandmarks(time, landmarks);
                } catch (IOException e) {
                    LOGGER.error("IOException during query", e);
                }
            }
        });
    }

    private void queryLandmarks(double time, Collection<Landmark> landmarks) throws IOException {
        LOGGER.info("landmarks query");
        List<RecognizedSong> recognizedSongs = serverClient.makeQuery(serializeLandmarks(landmarks));
        LOGGER.info("number of recognized songs: {}", recognizedSongs.size());
        for (RecognizedSong recognizedSong : recognizedSongs) {
            LOGGER.info("time: {}, recognized song: {}", time, recognizedSong);
        }
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);

        Double duration = DEFAULT_DURATION;
        Double interval = DEFAULT_INTERVAL;
        Integer nOffsets = DEFAULT_OFFSETS;
        String token = null;
        String hostname = null;

        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-duration")) {
                checkHasNextArg(i, args);
                duration = Double.valueOf(args[++i]);
            } else if (args[i].equals("-interval")) {
                checkHasNextArg(i, args);
                interval = Double.valueOf(args[++i]);
            } else if (args[i].equals("-offsets")) {
                checkHasNextArg(i, args);
                nOffsets = Integer.valueOf(args[++i]);
            } else if (args[i].equals("-token")) {
                checkHasNextArg(i, args);
                token = args[++i];
            } else if (args[i].equals("-server")) {
                checkHasNextArg(i, args);
                hostname = args[++i];
            } else {
                help();
            }
        }

        if (token == null || hostname == null) {
            help();
        }

        try {
            new Launcher(duration, interval, nOffsets, token, hostname).run();
        } catch (IOException e) {
            LOGGER.error("IO Exception", e);
        } catch (InterruptedException e) {
            LOGGER.error("Interrupted exception", e);
        }
    }

    private static void help() {
        System.err.println("Parameters:                                        ");
        System.err.println(" [-duration x]     - landmark duration time        ");
        System.err.println(" [-interval x]     - interval between landmarks    ");
        System.err.println(" [-offsets x]      - a number of landmark offsets  ");
        System.err.println("  -token token     - a token to be used with server");
        System.err.println("  -server hostname - a server hostname             ");
        System.exit(1);
    }

    private static void checkHasNextArg(int i, String[] args) {
        if (i >= args.length - 1) {
            help();
        }
    }
}
