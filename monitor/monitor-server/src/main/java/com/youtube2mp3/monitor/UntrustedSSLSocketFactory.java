package com.youtube2mp3.monitor;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;

public class UntrustedSSLSocketFactory extends SSLSocketFactory {
    SSLContext sslContext = SSLContext.getInstance("TLS");

    public static HttpClient getUntrustedHttpClient(ClientConnectionManager ccm) {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[] { new UntrustedX509TrustManager() }, new SecureRandom());
            HttpClient client = new DefaultHttpClient();
            SSLSocketFactory ssf = new UntrustedSSLSocketFactory(ctx);
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", ssf, 443));
            DefaultHttpClient sslClient = new DefaultHttpClient(ccm, client.getParams());
            return sslClient;
        } catch (GeneralSecurityException ex) {
            throw new RuntimeException(ex);
        }
    }

    public UntrustedSSLSocketFactory(KeyStore truststore) throws GeneralSecurityException {
        super(truststore);
        TrustManager tm = new UntrustedX509TrustManager();
        sslContext.init(null, new TrustManager[] { tm }, null);
    }

    public UntrustedSSLSocketFactory(SSLContext context) throws GeneralSecurityException {
        super((KeyStore) null);
        sslContext = context;
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException,
            UnknownHostException {
        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }
}