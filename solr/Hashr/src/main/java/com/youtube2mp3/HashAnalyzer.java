package com.youtube2mp3;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.util.Version;

/**
 * An analyzer for a fingerprint hash field that has the form:
 * 
 * <pre>
 * <hash> <offset> <hash> <offset>...
 * </pre>
 */
public class HashAnalyzer extends Analyzer {

    protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        Tokenizer source = new WhitespaceTokenizer(Version.LUCENE_46, reader);
        return new TokenStreamComponents(source, new HashFilter(source));
    }
}
