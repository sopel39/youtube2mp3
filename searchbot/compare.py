'''
Created on 27 sty 2014

@author: sopel39
'''

from utils.string import remove_parenthesis, cleanup


def words_similarity(s1, s2):
    s1_words = cleanup(remove_parenthesis(s1.lower())).split()
    s2_words = cleanup(remove_parenthesis(s2.lower())).split()
    total = len(s1_words) + len(s2_words)
    same = 0
    for word in s1_words:
        same += 1 if word in s2_words else 0
    for word in s2_words:
        same += 1 if word in s1_words else 0
    return float(same) / total if total else 0.

def lengths_similarity(l1, l2):
    return 1. - (float(abs(l1 - l2)) / max(l1, l2)) if l1 and l2 else 0.
