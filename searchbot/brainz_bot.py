'''
Created on 26 sty 2014

@author: sopel39
'''
from datetime import date, timedelta
import logging
import time

from lxml import etree
import requests

from model import Brainz
from settings import searchbot as settings
from youtube import queue_youtube_match


logger = logging.getLogger(__name__)
index_logger = logging.getLogger('searchbot_index')

def get_brainz_recordings(country, brainz_date):
    # make GET request to Brainz DB instance
    url = '{0}?query="" AND country:{1} AND date:{2}'.format(settings.BRAINZ_URL, country, brainz_date)
    response = requests.get(url)
    response.raise_for_status()

    # parse recordings from result XML
    root = etree.fromstring(response.text)
    ns = {'ns': settings.BRAINZ_NAMESPACE}
    recordings = root.xpath('/ns:metadata/ns:recording-list/ns:recording', namespaces=ns)
    results = []
    for recording in recordings:
        try:
            recording_id = recording.xpath('@id')[0]
            title = recording.xpath('ns:title/text()', namespaces=ns)[0]
            artist = recording.xpath('ns:artist-credit/ns:name-credit/ns:artist/ns:name/text()', namespaces=ns)[0]
            release = recording.xpath('ns:release-list/ns:release/ns:title/text()', namespaces=ns)[0]
            length = recording.xpath('ns:length/text()', namespaces=ns)
            # sometimes there might be no length available
            if not length:
                logger.warn("no length for brainz title: %s, artist: %s, release: %s, country: %s, brainz_date: %s",
                            title, artist, release, country, brainz_date)
                continue
            length = int(length[0])
            results.append({'recording_id': recording_id, 'title': title, 'artist': artist,
                            'release': release, 'length': length})
        except:
            # don't crash because of a single incorrect recording
            logger.exception('could not parse Brainz recording:\n%s', etree.tostring(recording, pretty_print=True))

    return results

def scan_brainz_date(country, brainz_date, update_threshold, brainz, stop_event):
    # update curr_date recordings only if it wasn't updates for update_threshold days
    update_date = brainz.get_update_date(country, brainz_date)
    if (not update_date) or (date.today() - update_date).days >= update_threshold:
        logger.info('updating brainz database for country: %s, date: %s', country, brainz_date)

        try:
            # add a fingerprint from youtube for each recording for a given day
            recordings = get_brainz_recordings(country, brainz_date)
            for recording in recordings:
                # check if we got stop signal
                if stop_event.is_set():
                    return
                queue_youtube_match(recording, country, brainz_date)

            # mark brainz database for a given date updated
            brainz.set_update_date(country, brainz_date, date.today())
            time.sleep(settings.COOLDOWN_SLEEP_TIME)
        except:
            logger.exception('failed to update brainz database for country: %s, date: %s', country, brainz_date)
    else:
        logger.info('skipping brainz database for country: %s, date: %s', country, brainz_date)

def brainz_bot(country, start_date, end_date, update_threshold, stop_event):
    # iterate brainz database for a given country from start_date to end_date
    with Brainz() as brainz:
        curr_date = start_date
        while end_date <= curr_date and (not stop_event.is_set()):
            scan_brainz_date(country, curr_date, update_threshold, brainz, stop_event)
            curr_date -= timedelta(days=1)
