'''
Created on 26 sty 2014

@author: sopel39
'''
from datetime import date, timedelta
import logging
from logging.config import dictConfig
import signal
import threading

from apscheduler.scheduler import Scheduler

import brainz_bot
from settings import searchbot as settings
from settings.logging_common import get_basic_configuration
import youtube


# setup bot wide logging
dictConfig(get_basic_configuration('searchbot.log'))
logger = logging.getLogger(__name__)

sched = Scheduler()
stop_event = threading.Event()

def full_scan_job():
    brainz_bot.brainz_bot(settings.COUNTRY,
                          date.today(), settings.FULL_SCAN_END_DATE,
                          settings.FULL_SCAN_DAYS_THRESHOLD, stop_event)

@sched.cron_schedule(hour=settings.DAILY_SCAN_HOUR)
def daily_scan_job():
    start_date = date.today()
    end_date = start_date - timedelta(days=settings.DAILY_SCAN_DAYS_RANGE)
    brainz_bot.brainz_bot(settings.COUNTRY, start_date, end_date,
                          settings.DAILY_SCAN_DAYS_THRESHOLD, stop_event)

# setup graceful exit on signals
def exit_handler(signum, frame):
    stop_event.set()
signal.signal(signal.SIGINT, exit_handler)

if __name__ == '__main__':
    # start youtube workers
    youtube.start_youtube_match_workers()

    # schedule tasks
    sched.start()
    while not stop_event.is_set():
        full_scan_job()
    logger.info('full scan job finished')

    # wait for stop signal
    while not stop_event.is_set():
        signal.pause()
    sched.shutdown()
    youtube.stop_youtube_match_workers()
