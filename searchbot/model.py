'''
Created on 26 sty 2014

@author: sopel39
'''

import psycopg2

import settings.searchbot as settings


class Brainz(object):

    def __init__(self):
        self._conn = psycopg2.connect(settings.DB_CONN_STRING)

        # create tables
        with self._conn, self._conn.cursor() as cur:
            cur.execute("""
            CREATE TABLE IF NOT EXISTS brainz_updates(
                country varchar NOT NULL,
                brainz_date date NOT NULL,
                update_date date NOT NULL,
                PRIMARY KEY (country, brainz_date)
            )""")

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._conn.close()

    def get_update_date(self, country, brainz_date):
        with self._conn, self._conn.cursor() as cur:
            cur.execute("""
            SELECT update_date FROM brainz_updates
            WHERE country=%s AND brainz_date=%s
            """, (country, brainz_date))
            result = cur.fetchone()
            return result[0] if result else None

    def set_update_date(self, country, brainz_date, update_date):
        with self._conn, self._conn.cursor() as cur:
            cur.execute("""
            UPDATE brainz_updates SET update_date=%s
            WHERE country=%s and brainz_date=%s
            """, (update_date, country, brainz_date))
            cur.execute("""
            INSERT INTO brainz_updates
            SELECT %s, %s, %s
            WHERE NOT EXISTS (SELECT 1 FROM brainz_updates WHERE country=%s and brainz_date=%s);
            """, (country, brainz_date, update_date, country, brainz_date))

    def remove_entry(self, country, brainz_date):
        with self._conn, self._conn.cursor() as cur:
            cur.execute("""
            DELETE FROM brainz_updates
            WHERE country=%s and brainz_date=%s
            """, (country, brainz_date))

    def close(self):
        self._conn.close()
