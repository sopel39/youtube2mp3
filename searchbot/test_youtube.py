'''
Created on 13 maj 2014

@author: karol
'''
import os
import unittest

from mock import Mock

import database
from database.recordingsindex import RecordingsIndex, Recording
import youtube


class Test(unittest.TestCase):

    def setUp(self):
        # return some recordings in search_youtube
        youtube.search_youtube = Mock(return_value=[
            {'id': 'foo', 'title': 'video1', 'length': 1},
            {'id': 'bar', 'title': 'video2', 'length': 1},
            {'id': 'zoo', 'title': 'video1', 'length': 1}
        ])

        database.youtube.ytdownloader.youtube_download = Mock(return_value='/tmp/mock_path')
        database.youtube.fingerprint.add_fingerprint_to_database = Mock()
        os.remove = Mock()

        # initialize index
        self._index = RecordingsIndex()
        self._index.delete_recording('brainz_recording_id')

    def tearDown(self):
        self._index.close()

    def _get_sample_recording(self, recording_title):
        return {
            'recording_id': 'recording_id',
            'artist': '',
            'release': '',
            'length': 1,
            'title': recording_title
        }

    def _get_sample_match(self, ignore):
        return {
            RecordingsIndex.NAME: '',
            RecordingsIndex.TYPE: '',
            RecordingsIndex.URL: '',
            RecordingsIndex.IGNORE: ignore
        }

    def test_should_add_youtube_match(self):
        youtube.add_youtube_match(self._get_sample_recording('video1'), None, None, self._index, None)
        matches = self._index.get_recording('brainz_recording_id')[RecordingsIndex.MATCHES]
        self.assertEqual(len(matches), 1)
        self.assertTrue('youtube_foo' in matches)
        self.assert_downloaded('foo')

    def test_should_add_empty_recording(self):
        youtube.add_youtube_match(self._get_sample_recording('video3'), None, None, self._index, None)
        matches = self._index.get_recording('brainz_recording_id')[RecordingsIndex.MATCHES]
        self.assertEqual(len(matches), 0)
        self.assert_not_downloaded()

    def test_should_skip_matched_recording(self):
        rec = Recording.of('brainz_recording_id', '', '')
        rec[RecordingsIndex.MATCHES]['youtube_xyz'] = self._get_sample_match(False)
        self._index.add_recording(rec)

        youtube.add_youtube_match(self._get_sample_recording('video1'), None, None, self._index, None)
        matches = self._index.get_recording('brainz_recording_id')[RecordingsIndex.MATCHES]
        self.assertEqual(len(matches), 1)
        self.assertTrue('youtube_xyz' in matches)
        self.assert_not_downloaded()

    def test_should_skip_ignored_matches(self):
        rec = Recording.of('brainz_recording_id', '', '')
        rec[RecordingsIndex.MATCHES]['youtube_foo'] = self._get_sample_match(True)
        self._index.add_recording(rec)

        youtube.add_youtube_match(self._get_sample_recording('video1'), None, None, self._index, None)
        matches = self._index.get_recording('brainz_recording_id')[RecordingsIndex.MATCHES]
        self.assertEqual(len(matches), 2)
        self.assertTrue('youtube_foo' in matches)
        self.assertTrue(matches['youtube_foo'][RecordingsIndex.IGNORE])
        self.assertTrue('youtube_zoo' in matches)
        self.assert_downloaded('zoo')

    def assert_downloaded(self, youtube_id):
        database.youtube.ytdownloader.youtube_download.delay.assert_called_once_with(youtube_id, None)
        self.assertEqual(database.youtube.fingerprint.add_fingerprint_to_database.delay.call_count, 1);

    def assert_not_downloaded(self):
        self.assertFalse(database.youtube.ytdownloader.youtube_download.delay.called)
        self.assertFalse(database.youtube.fingerprint.add_fingerprint_to_database.delay.called)
