'''
Created on 26 sty 2014

@author: sopel39
'''
from datetime import date, timedelta
import unittest

from model import Brainz


class Test(unittest.TestCase):

    def setUp(self):
        self._brainz = Brainz()

    def tearDown(self):
        self._brainz.close()

    def test_should_get_check_date(self):
        today = date.today()
        update_date = today
        self._brainz.remove_entry('PL', today)
        self.assertIsNone(self._brainz.get_update_date('PL', today))
        self._brainz.set_update_date('PL', today, update_date)
        self.assertEqual(self._brainz.get_update_date('PL', today), update_date)
        update_date2 = update_date + timedelta(days=1)
        self._brainz.set_update_date('PL', today, update_date2)
        self.assertEqual(self._brainz.get_update_date('PL', today), update_date2)
        self.assertIsNone(self._brainz.get_update_date('UK', today))
