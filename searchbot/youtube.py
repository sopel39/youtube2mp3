'''
Created on 28 sty 2014

@author: sopel39
'''

from Queue import Queue
import logging
import threading

import compare
from database.recordingsindex import RecordingsIndex, Recording
from database.youtube import add_youtube_fingerprint, has_youtube_fingerprint
from settings import searchbot as settings
from settings.paths import TMP_BACKEND
from utils.string import remove_parenthesis, tuple_to_str
from utils.temporarydir import TemporaryDirectory
from utils.youtube import search_youtube


logger = logging.getLogger(__name__)
index_logger = logging.getLogger('searchbot_index')

def find_youtube_match(recording, index_recording):
    # check if a video is a good match
    recording_title = remove_parenthesis(recording['artist'] + ' ' + recording['title'])
    videos = search_youtube(recording_title)
    logger.debug('potential matches: %s found for recording: %s', videos, recording)
    for video in videos:
        words_sim = compare.words_similarity(recording_title, video['title'])
        lengths_sim = compare.lengths_similarity(recording['length'], video['length'])
        if (words_sim > settings.MATCH_WORDS_THRESHOLD and lengths_sim >= settings.MATCH_LENGTHS_THRESHOLD
            and not has_youtube_fingerprint(index_recording, video['id'])):
            return video
    return None

def add_youtube_match(recording, country, recording_date, index, download_dir):
    logger.info('matching recording: %s, country: %s, date: %s', recording, country, recording_date)

    # fetch existing index recording or create an empty one
    recording_id = 'brainz_' + recording['recording_id']
    recording_name = tuple_to_str((recording['artist'], recording['release'], recording['title']))
    recording_type = 'brainz'
    index_recording = index.merge_recording(Recording.of(recording_id, recording_name, recording_type))

    # check if index recording has some not ignored matches
    if index_recording.get_not_ignored_matches_count():
        logger.info('recording already indexed')
        return

    try:
        # try to match a youtube fingerprint for recording
        video = find_youtube_match(recording, index_recording)
        if video:
            logger.info('found youtube match: %s for recording: %s', video, recording)
            add_youtube_fingerprint(index_recording, video['id'], video['title'], download_dir)
            index_logger.info('added to index: %s, country: %s, date: %s', recording, country, recording_date)
            return
    except:
        logger.exception('could not get a fingerprint for recording: %s, country: %s, date: %s',
                         recording, country, recording_date)
    finally:
        # save index recording (either empty or with matched fingerprint)
        index.add_recording(index_recording)

    index_logger.info('not added to index: %s, country: %s, date: %s', recording, country, recording_date)

youtube_match_queue = Queue(1)
youtube_match_workers = []

def youtube_match_worker():
    def recordings_iter():
        return iter(youtube_match_queue.get, (True, None, None, None))

    with RecordingsIndex() as index, TemporaryDirectory(suffix='_ytdownloader', tmp_dir=TMP_BACKEND) as tmp_dir:
        # process recordings
        for _, recording, country, recording_date in recordings_iter():
            try:
                add_youtube_match(recording, country, recording_date, index, tmp_dir)
            except:
                logger.exception('add youtube match failed for recording: %s, country: %s, date: %s',
                                 recording, country, recording_date)

def start_youtube_match_workers():
    logger.info('starting youtube match workers')
    for _ in range(settings.YOUTUBE_MATCH_WORKERS):
        worker = threading.Thread(target=youtube_match_worker)
        worker.start()
        youtube_match_workers.append(worker)

def queue_youtube_match(recording, country, recording_date):
    youtube_match_queue.put((False, recording, country, recording_date))

def stop_youtube_match_workers():
    logger.info('stopping youtube match workers')
    # send stop message
    for _ in range(settings.YOUTUBE_MATCH_WORKERS):
        youtube_match_queue.put((True, None, None, None))
    for worker in youtube_match_workers:
        worker.join()
