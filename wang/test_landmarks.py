'''
Created on 25 cze 2014

@author: karol
'''
import unittest

from scipy.io import wavfile

from landmarks import find_landmarks


class Test(unittest.TestCase):

    def testLandmarks(self):
        _, data = wavfile.read('data/audios/bugs.wav')
        landmarks = find_landmarks(data[256:])
        print landmarks[-100:]
        print len(landmarks)
