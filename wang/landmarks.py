'''
Created on 4 lut 2014

@author: sopel39
'''

from scipy import signal
import scipy

from settings.wang import FFT_SIZE, HOP_SIZE, HIGHPASS_POLE, SPREAD_GAUSSIAN_STD, \
    MAX_PEAKS_PER_FRAME, DECAY_RATE, TIME_RANGE, BINS_RANGE, MAX_PAIRS_PER_PEAK
from utils import math


def spread_locmax(a, std, edge=None):
    # spread every local maxima using gaussian window
    result = scipy.zeros(len(a))
    for max_ind in math.locmax(a, edge=edge):
        result = math.spread(result, max_ind, a[max_ind], std)
    return result

def peak_bins_locmax(s, t):
    # peak bins are amplitude maximas
    peak_bins = math.locmax(s, edge=0.)
    # sort peaks by amplitude (descending)
    return peak_bins[(-s[peak_bins]).argsort()]

def find_peaks(data, threshold, data_range, peak_bins_finder):
    peaks = []
    # iterate over slices
    for t in data_range:
        s = data[t]
        # get peak bins for that slice
        peak_bins = peak_bins_finder(s, t)
        # store peaks
        added_peaks = 0
        for peak_bin in peak_bins:
            peak_ampl = s[peak_bin]
            # check if we reached limit of peaks per slice
            if added_peaks >= MAX_PEAKS_PER_FRAME:
                break;
            # check if a peak reached threshold
            if peak_ampl <= threshold[peak_bin]:
                continue
            # store peak and update threshold
            peaks += [(t, peak_bin)]
            threshold = math.spread(threshold, peak_bin, peak_ampl, SPREAD_GAUSSIAN_STD)
            added_peaks += 1
        # threshold decay
        threshold *= DECAY_RATE
    return scipy.array(peaks)

def peaks_to_landmarks(peaks):
    landmarks = []
    for t, peak_bin in peaks:
        # find all nearby peaks
        pair_peaks = peaks[
            (peaks[:, 0] > t)
            & (peaks[:, 0] < t + TIME_RANGE + 1)
            & (peaks[:, 1] > peak_bin - BINS_RANGE)
            & (peaks[:, 1] < peak_bin + BINS_RANGE)]
        pair_peaks = pair_peaks[:MAX_PAIRS_PER_PEAK]
        # add landmarks
        for t2, peak_bin2 in pair_peaks:
            landmarks.append((t, t2, peak_bin, peak_bin2))
    return scipy.array(landmarks)

def find_landmarks(data):
    # first compute stft
    data = math.stft(data, scipy.hanning(FFT_SIZE), FFT_SIZE, HOP_SIZE)
    data = scipy.absolute(data)
    if not len(data):
        return []
    # convert to log domain
    data = scipy.log(scipy.maximum(data, scipy.amax(data) / 1e6))
    # run highpass filter
    data -= scipy.mean(data)
    data = signal.lfilter([1, -1], [1, -HIGHPASS_POLE], data, 0)
    # compute initial threshold based on the first 10 samples in each bin
    threshold = spread_locmax(scipy.amax(data[:10], 0), SPREAD_GAUSSIAN_STD, 0.)
    # forward peaks search
    peaks = find_peaks(data, threshold, range(len(data)), peak_bins_locmax)
    if not len(peaks):
        return []
    # backward peaks pruning
    threshold = scipy.zeros(data.shape[1])
    peaks = find_peaks(data, threshold, reversed(range(len(data))),
                       lambda s, t: peaks[peaks[:, 0] == t][:, 1])
    peaks = scipy.flipud(peaks)
    # return landmarks
    return peaks_to_landmarks(peaks)
