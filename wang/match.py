'''
Created on 11 lut 2014

@author: sopel39
'''

import logging

from scipy import stats
import scipy

from landmarks import find_landmarks
from settings.wang import HOP_SIZE, MATCH_HITS_THRESHOLD, MATCH_RATIO_THRESHOLD


logger = logging.getLogger(__name__)

def filter_fingerprints(fingerprints):
    results = []
    for (recording_id, fingerprint_id, _), times in fingerprints.items():
        offsets = times[:, 1]
        logger.debug('fingerprint id: %s, offsets: %s', fingerprint_id, offsets)
        # find offsets mode
        offsets_mode = stats.mode(offsets)[0][0]
        # count every match within one unit of mode
        hits = scipy.sum(scipy.absolute(offsets - offsets_mode) <= 1)
        results.append((fingerprint_id, recording_id, hits, len(offsets)))
    # sort results by count
    results = sorted(results, key=lambda result: (result[2], result[3]), reverse=True)
    logger.info('filter results: %s' % results)
    return results

def filter_matches(matches):
    # filter matches by hits threshold
    matches = filter(lambda (fingerprint_id, recording_id, hits, total_hits): hits > MATCH_HITS_THRESHOLD, matches)
    # filter matches by ratio between first and the second result
    if len(matches) == 1 or (len(matches) >= 2 and matches[0][2] * MATCH_RATIO_THRESHOLD >= matches[1][2]):
        return [matches[0]]
    return []

def find_offsetted_landmarks(data):
    # find landmarks with quarter hop data offsets for better matching
    return reduce(
        lambda landmarks, i: scipy.vstack((landmarks, find_landmarks(data[HOP_SIZE * i / 4:]))),
        range(4), scipy.empty((0, 4), dtype=int))

def match_landmarks(landmarks, index):
    return filter_matches(filter_fingerprints(index.find_fingerprints(landmarks)))
