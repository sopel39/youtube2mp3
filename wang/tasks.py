'''
Created on 11 lut 2014

@author: sopel39
'''

import logging
import os

import scipy.io.wavfile

from app import app
from database.wangindex import WangIndex
from landmarks import find_landmarks
from match import find_offsetted_landmarks
import match
from settings.celery import WANG_WATCHER_QUEUE, WANG_MATCHER_QUEUE
from settings.paths import TMP_CLIENT, TMP_BACKEND
import settings.wang as settings
from utils import audio, file
from utils.temporarydir import TemporaryDirectory


logger = logging.getLogger(__name__)

def copy_wav(path, dst_dir):
    # convert to wav with correct sample rate
    basename = file.basename_no_suffix(path)
    wav_path = os.path.join(dst_dir, basename + '.wav')
    return audio.convert_to_wav(path, False, wav_path, sample_rate=settings.SAMPLE_RATE, tmp_dir=dst_dir)

@app.task(queue=WANG_WATCHER_QUEUE)
def add_fingerprint_to_database(fingerprint_id, recording_id, path):
    with WangIndex() as index, TemporaryDirectory(tmp_dir=TMP_BACKEND) as tmp_dir:
        # load audio data
        wav_path = copy_wav(path, tmp_dir)
        _, data = scipy.io.wavfile.read(wav_path)
        # split data into parts
        parts = [data[i:i + settings.PART_SIZE] for i in range(0, len(data), settings.PART_HOP)]
        # compute landmarks for each part
        part_landmarks = [(str(n), find_landmarks(part)) for n, part in enumerate(parts)]
        # add all parts to index
        index.add_hashes_parts(recording_id, fingerprint_id, part_landmarks)

@app.task(queue=WANG_MATCHER_QUEUE)
def match_fingerprint(path):
    with WangIndex() as index, TemporaryDirectory(tmp_dir=TMP_CLIENT) as tmp_dir:
        # load audio data
        wav_path = copy_wav(path, tmp_dir)
        _, data = scipy.io.wavfile.read(wav_path)
        # match landmarks with database
        matches = match.match_landmarks(find_offsetted_landmarks(data), index)
        logger.info('match results: %s' % matches)
        return [(fingerprint_id, recording_id) for fingerprint_id, recording_id, _, _ in matches]

@app.task(queue=WANG_MATCHER_QUEUE)
def match_landmarks(landmarks):
    with WangIndex() as index:
        # match landmarks with database
        matches = match.match_landmarks(landmarks, index)
        logger.info('match results: %s' % matches)
        return [(fingerprint_id, recording_id) for fingerprint_id, recording_id, _, _ in matches]

@app.task(queue=WANG_WATCHER_QUEUE)
def remove_fingerprint(fingerprint_id, recording_id):
    with WangIndex() as index:
        index.remove_fingerprint(recording_id, fingerprint_id)

@app.task(queue=WANG_WATCHER_QUEUE)
def remove_fingerprints(recording_id):
    with WangIndex() as index:
        index.remove_fingerprints(recording_id)
