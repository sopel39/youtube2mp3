'''
Created on 17 sty 2014

@author: sopel39
'''

from logging.config import dictConfig
import sys

from celery import Celery
from celery.signals import celeryd_init

from settings.logging_common import get_basic_configuration


app = Celery('wang', include=['wang.tasks'])
app.config_from_object('settings.celery')

@celeryd_init.connect
def configure_worker(**kwargs):
    # setup worker logging
    dictConfig(get_basic_configuration('wang.log'))

if __name__ == '__main__':
    app.start(sys.argv)
