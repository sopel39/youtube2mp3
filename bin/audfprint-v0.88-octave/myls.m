function F = myls(D)
try
  S = ls('-1', D);
catch
  S = '';
end
F = [];
n = 1;
for i = 1:size(S,  1)
  F{n} = strtrim([D, '/', S(i,:)]);
  n=n+1;
end
