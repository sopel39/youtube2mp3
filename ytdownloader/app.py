'''
Created on 29 sty 2014

@author: sopel39
'''

from logging.config import dictConfig
import sys

from celery import Celery
from celery.signals import celeryd_init

from settings import ytdownloader as settings
from settings.logging_common import get_basic_configuration


app = Celery('ytdownloader', include=['ytdownloader.tasks'])
app.config_from_object('settings.celery')
app.conf.update(
    CELERYD_CONCURRENCY=settings.NUMBER_OF_WORKERS
)

@celeryd_init.connect
def configure_worker(**kwargs):
    # setup worker logging
    dictConfig(get_basic_configuration('ytdownloader.log'))

if __name__ == '__main__':
    app.start(sys.argv)
