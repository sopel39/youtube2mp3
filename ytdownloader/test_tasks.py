'''
Created on 18 maj 2014

@author: sopel39
'''
import os
import unittest

import tasks
from utils.temporarydir import TemporaryDirectory


class Test(unittest.TestCase):

    def test_youtube_download(self):
        with TemporaryDirectory() as tmp_dir:
            filename = tasks.youtube_download('jNQXAC9IVRw', tmp_dir)
            files = os.listdir(tmp_dir)
            self.assertEqual(len(files), 1)
            self.assertEqual(os.path.join(tmp_dir, files[0]), filename)
