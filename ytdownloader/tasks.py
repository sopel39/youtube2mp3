'''
Created on 29 sty 2014

@author: sopel39
'''

import logging
import os
import subprocess

from app import app
import settings.ytdownloader as settings
from utils import file
from utils.temporarydir import TemporaryDirectory
from utils.timeout import timeout


logger = logging.getLogger(__name__)

@timeout(settings.TIMEOUT)
def youtube_dl_quality(quality, download_dir, youtube_id):
    try:
        params = [settings.YOUTUBEDL_BIN, '-q']
        if quality:
            params += ['-f', quality]
        params += ['-o', os.path.join(download_dir, '%(id)s.%(ext)s'),
                   'http://www.youtube.com/watch?v=' + youtube_id]
        subprocess.check_call(params)
        # successfully downloaded youtube video
        return True
    except:
        return False

def youtube_dl(qualities, download_dir, youtube_id):
    for quality in qualities:
        logger.info('downloading youtube video: %s, with quality: %s', youtube_id, quality)
        if youtube_dl_quality(quality, download_dir, youtube_id):
            logger.info('youtube video: %s downloaded', youtube_id)
            return
    raise Exception('could not download youtube video: ' + youtube_id)

@app.task()
def youtube_download(youtube_id, download_dir):
    # use temporary directory to cleanup any part files
    with TemporaryDirectory(tmp_dir=download_dir) as tmp_dir:
        logger.info('downloading youtube: %s', youtube_id)
        # try 'wostaudio', otherwise use 'worst' and default
        youtube_dl(['worstaudio', 'worst', None], tmp_dir, youtube_id)
        # detect full filename with extension
        for filename in os.listdir(tmp_dir):
            if file.basename_no_suffix(filename) == youtube_id:
                # copy file to download directory
                tmp_filename = os.path.join(tmp_dir, filename)
                dst_filename = os.path.join(download_dir, filename)
                os.rename(tmp_filename, dst_filename)
                return dst_filename
        raise Exception('youtube video not downloaded: ' + youtube_id)

